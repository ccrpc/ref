---
title: Home
draft: false
bannerHeading: "Regional Environmental Framework"
bannerText: >
  The REF serves to strengthen environmental stewardship in the transportation planning process by identifying and informing decisions concerning environmental resources and areas potentially impacted by transportation projects.
---

## Regional Environmental Framework

### Welcome to the Champaign County Regional Environmental Framework (REF)

The REF began development in May 2019, with its [first phase report published in November 2020.](https://www.ccrpc.org/transportation/regional_environmental_framework_(ref).php) The second phase, the Web-based REF, began in March of 2021 and includes all updated REF content, as well as an interactive map with resource
and compliance analysis tools developed specifically for Champaign County. As
an interactive online planning resource, all REF content is contained in this
public website rather than hardcopy. The Web-based REF allows for more
interactive content with links to related resources, downloadable data, and
interactive maps. Explore the plan by clicking on the section drop-down tabs at the top of the
page which are also summarized in the menu below.

**Project Lead:** Champaign-Urbana Urbanized Area Transportation Study (CUUATS)

**Project Timeline:** May 2019 - December 2022

**Project Funding:** Statewide Planning and Research funds from the Federal Highway Administration (FHWA) and the Illinois Department of Transportation (IDOT)

**Project Working Group:**

- Illinois Natural History Survey
- Illinois Department of Transportation
- Illinois State Geological Survey
- Illinois State Archaeological Survey
- University of Illinois Facilities and Services
- Champaign County Soil and Water Conservation District
- Champaign County Forest Preserve District
- Champaign County Department of Planning and Zoning
- Champaign Park District
- City of Champaign Department of Planning and Development
- City of Urbana Planning Department
- Urbana Park District
- Resource Environmental Solutions, LLC

#### Updates from REF Phase 1 Report

- Made all data and analysis from the Phase 1 Report, maps, and other pertinent layers available to users in a user-friendly, online resource.
- Leveraged CUUATS’ existing web mapping tools to display inventory of resources in a more accessible and functional way through the [Interactive Map](https://ccrpc.gitlab.io/ref/overview/interactive-map/).
- Developed [Composite Scoring system](https://ccrpc.gitlab.io/ref/data/) aggregating attributes of included resources to identify potentially ecologically vulnerable areas within Champaign County.
- Expanded resource selection to capture broader range of significant and potentially review-triggering resources: [Agricultural Land](https://ccrpc.gitlab.io/ref/resources/topography/#agricultural-land), [Historic Districts](https://ccrpc.gitlab.io/ref/resources/cultural/#historic-districts).
- Expanded analysis of previously included resources to facilitate environmental review processes and improve overall environmental outcomes: [Soil Development Potential](https://ccrpc.gitlab.io/ref/resources/topography/#soil), [Grant Funded Sites](https://ccrpc.gitlab.io/ref/resources/wildlife/#grant-funded-sites).
