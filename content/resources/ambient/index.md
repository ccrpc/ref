---
title: "Ambient Environment"
draft: false
weight: 60
bannerHeading: Ambient Environment
bannerText: >
  Air quality, light pollution, and noise pollution are all salient concerns when considering potential impacts of new transportation projects.
---

## Air Quality
Transportation contributes to three important categories of air pollutants: 
- Greenhouse Gases ([GHG](https://www.epa.gov/ghgemissions/overview-greenhouse-gases)): Gases that trap heat in the atmosphere, contributing to global climate change.
- Criteria Air Pollutants ([CAP](https://www2.illinois.gov/epa/topics/air-quality/air-quality-reports/Documents/2018%20Annual%20Air%20Quality%20Report%20Final.pdf)): Common ambient air pollutants that cause harm to health, the environment, and property
- Mobile Source Air Toxics ([MSATs](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf)): Air toxics emitted from transportation sources such as highway vehicles and non-road equipment that are known or suspected to cause cancer or other serious health and environmental effects.

Under the Clean Air Act, the U.S. EPA set National Ambient Air Quality Standards (NAAQS) for the six CAPs deemed most harmful to public health and the environment: 
- Particulate matter (PM₂․₅, PM₁₀) 
- Nitrogen dioxide (NO₂) 
- Ozone (O₃)
- Carbon monoxide (CO)
- Sulfur dioxide (SO₂) 
- Lead (Pb)

The following NAAQS must be met by local governments in Illinois, which requires planners to consider the air quality impacts that may result from a project or program:

{{<image src="standard.jpg"
  alt="Figure showing Summary of Illinois and National Ambient Air Quality Standards"
  attr="IEPA, Illinois 2018 Annual Air Quality Report Final, 2018" attrlink=""
  position="full">}}

Planners need to document considerations to the following types of air quality impacts:
- Impacts to nonattainment areas: Does the project occur in an area not meeting NAAQS, and if so, does it confirm to air quality plans established in the State Implementation Plan (SIP)?
>* [Nonattainment Areas for Criteria Pollutions (Green Book)](https://www.epa.gov/green-book)
>* [EPA General Conformity Rule](https://www.epa.gov/general-conformity/what-general-conformity)
- Impacts to local air quality: Any potential impacts to local air quality such as along the ROW, construction zones, and overall increases in vehicle miles traveled (VMT).
- GHG Impacts: Emissions generated from the different alternatives, including no-build.

## Light Pollution
Light pollution is the inappropriate or excessive use of artificial lighting and
is linked to human health problems (such as depression and insomnia) and disrupts
the fundamental biological activity of plant and animal species ([IDA, n.d.](https://www.darksky.org/light-pollution/))
([Falchi et al., 2019](https://pubmed.ncbi.nlm.nih.gov/31362173/)).   No national standards exist to regulate the impacts of
light pollution, but local nuisance ordinances are often sited in limiting
excessive night-time lighting. 

Planners can minimize light pollution through best practices, such as choosing
outdoor lighting fixtures that minimize glare while reducing light trespass and
skyglow.  Options can be found [here](https://www.darksky.org/our-work/lighting/lighting-for-industry/fsa/fsa-products/). Other best practices include the following: 

Light Pollution Control Best Practices
{{<image src="light.jpg"
  alt="Figure showing Light Pollution Control Best Practices"
  attr="International Dark Sky Association – Light Pollution Solution Postcard))" attrlink=""
  position="full">}}

## Noise Pollution
Damage to human ears from noise begins roughly at 85 decibels, and for wildlife
the level varies by species. Noise above 85 decibels for a prolonged period of
time can damage hearing, while noises above 120 decibels can cause immediate
harm ([HealthLink, n.d.](https://www.healthlinkbc.ca/)). Typical highways range from 70-80 decibels, potentially impacting both humans and wildlife.  

{{<image src="decibel.jpg"
  alt="Figure showing decible Level and Responses from Industry Noise Sources"
  attr="Decible Level and Responses from Industry Noise Sources)" attrlink=""
  position="full">}}

The Federal Highway Administration (FHWA) deals with noise pollution using three different [methods](https://highways.dot.gov/public-roads/julyaugust-2003/living-noise):
- Source Control: Decibel limitations on newly manufactured trucks with a weight limit
- Design or Operation Mitigation: Restricting truck access, traffic signal timing adjustments, depressing the highway below grade, and installing noise barriers (this is the most common method).
- Noise-Compatible Land Use Planning: Locating roads and highways away from sensitive areas

Planners need to document considerations to the following types of noise impacts:
- Impacts to quality of life and wildlife within 500 feet of project during construction and during operation of all project alternatives
- Impacts to sensitive noise receptors in the project area 
<rpc-table
url="noise.csv"
table-title="Example Sensitive Noise Receptors"
source="IDOT Highway Traffic Noise Assessment Manual, May 2017"
source-url="">
</rpc-table>

Note: This listing is not exhaustive.  See IDOT Highway Traffic Noise Assessment Manual for full listing
>*Category A requires FHWA “Test for Meeting Activity Category A Designation”
>*Category D only applies when there are no exterior activities affected

Noise analyses must be conducted for all Type I projects (see [23 CFR 772](https://www.ecfr.gov/current/title-23/chapter-I/subchapter-H/part-772)).
Noise pollutions resources can be found here:
- [IDOT Highway Traffic Noise Analysis](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Environment/Highway%20Traffic%20Noise%20--%20Noise%20Analysis%20111215.pdf)
- [Bureau of Local Roads and Streets Manual - Ch 20-6: Noise Analysis](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Local-Roads-and-Streets/Local%20Roads%20and%20Streets%20Manual.pdf)
- [Bureau of Design and Environment Manual - Ch 26-6.05(d): Noise Analysis](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf)
