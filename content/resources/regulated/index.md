---
title: "Regulated Sites"
draft: false
weight: 50
bannerHeading: Regulated Sites
bannerText: >
  It is important to consider sites that currently or have at one time generated, transported, stored, or disposed of regulated substances, including spills, in transportation planning.
---

The Illinois Environmental Protection Agency ([IEPA, 2022](https://www2.illinois.gov/epa/topics/waste-management/waste-disposal/special-waste/Pages/default.aspx)) defines special waste as any
potentially infectious medical waste, hazardous waste, pollution control waste
or industrial process waste. Potential presence of hazardous or regulated
substances affects both human and ecological health. Work in or around any
identified special waste sites can cause a release of contaminants into the air,
soil, and/or water. 

{{<image src="special_waste.jpg"
  alt="Figure describes definition of special waste"
  attr="Special Waste" attrlink="https://www.asantewm.com/for-business/disposal-recycling-services/special-waste/"
  position="right">}}

Federal and state regulations require that all currently known and potential special waste or other regulated sites be identified as part of the environmental review proces, so that special preparations can be made to handle contaminants appropriately.
The Illinois Dept. of Transportation (IDOT) environmental review process requires identifying any of the following sites up to one mile from a potential project site during the planning stage:
- Underground Storage Tank ([UST](https://www.epa.gov/ust/learn-about-underground-storage-tanks-usts)): a tank and any underground piping connected to the tank that has at least 10 percent of its combined volume underground.
- Leaking Underground Storage Tank ([LUST](https://www.epa.gov/ust/leaking-underground-storage-tanks-corrective-action-resources#intro)): Release of a fuel product from a UST.
- [Brownfields](https://www2.illinois.gov/epa/topics/cleanup-programs/brownfields/Pages/default.aspx): properties where the potential presence of a hazardous substance, pollutant, or contaminant limits reuse or redevelopment.
- Superfund Sites ([CERCLA](https://www.epa.gov/superfund/superfund-cercla-overview)): Sites that are contaminated with hazardous waste that qualify for national funding and cleanup priority due the severity and danger/scale of the contamination.
- [Landfills](https://www.epa.gov/landfills/basic-information-about-landfills): Facilities that have permits to treat, store, and dispose of certain hazardous and nonhazardous wastes.
- Cleanup Sites from the Site Remediation Program ([SRP](https://www2.illinois.gov/epa/topics/cleanup-programs/srp/Pages/overview.aspx)): Contaminated sites where IEPA assistance is needed to reach targeted or required cleanup goals.
- Resource Conservation and Recovery Act ([RCRA](https://www.epa.gov/laws-regulations/summary-resource-conservation-and-recovery-act)) Sites: Facilities involved in the generation, transport, treatment, storage, or disposal of hazardous and other solid wastes.
- RCRA Sites subject to corrective action ([CORRACTS](https://www.epa.gov/hw/learn-about-corrective-action#whatare)): A subsection of RCRA facilities are those that are subject to corrective action.
- Emergency Response Notification System ([ERNS](https://nepis.epa.gov/Exe/ZyNET.exe/9100O2XE.TXT?ZyActionD=ZyDocument&Client=EPA&Index=1995+Thru+1999&Docs=&Query=&Time=&EndTime=&SearchMethod=1&TocRestrict=n&Toc=&TocEntry=&QField=&QFieldYear=&QFieldMonth=&QFieldDay=&IntQFieldOp=0&ExtQFieldOp=0&XmlQuery=&File=D%3A%5Czyfiles%5CIndex%20Data%5C95thru99%5CTxt%5C00000028%5C9100O2XE.txt&User=ANONYMOUS&Password=anonymous&SortMethod=h%7C-&MaximumDocuments=1&FuzzyDegree=0&ImageQuality=r75g8/r75g8/x150y150g16/i425&Display=hpfr&DefSeekPage=x&SearchBack=ZyActionL&Back=ZyActionS&BackDesc=Results%20page&MaximumPages=1&ZyEntry=1&SeekPage=x&ZyPURL)) Sites: Instances of oil discharges and hazardous substance releases.

{{<image src="chanute.jpg"
  alt="Figure showing Chanute Air Force Base Cleanup"
  attr="CERCLA - Chanute Air Force Base Cleanup" attrlink="https://twitter.com/therantoulpress/status/1067481142369107968"
  position="right">}}

IDOT Level 2 Special Waste Screening specifies the following search
distances for each category of site.  If a site is within the minimum search
distance from the project, a Preliminary Environmental Site Assessment (PESA) is
required.

<rpc-table
url="sp_criteria.csv"
table-title="IDOT Level 2 Special Waste Screening Criteria"
source="IDOT Special Waste Screening Form"
source-url="https://apps.dot.illinois.gov/environment/instructions.pdf">
</rpc-table>

At the earliest possible time, planners need to identify and document all special waste sites within their respective minimum search distances.  Project alternatives can then be designed to avoid special waste sites or minimize potential releases/contact with these sites.
Many different databases were used in order to research accurate special waste data, including EPA Facility Registry Service, IEPA databases, other US EPA databases, and NETROnline.  Special waste databases do not all provide uniform or regularly updated information, so it may be necessary to utilize multiple to confirm locations and status of different sites.  Other resources that can be used are included in the REF Report.  Several important resources can be found using the following links:
>* [IDOT Environmental Survey Request (incl. Special Waste Screening Form)](https://apps.dot.illinois.gov/environment/instructions.pdf)
>* [IDOT Preliminary Site Assessment Manual](https://idot.illinois.gov/assets/uploads/files/doing-business/manuals-split/local-roads-and-streets/chapter%2020.pdf)
>* [Illinois EPA Special Waste Information](https://www2.illinois.gov/epa/topics/waste-management/waste-disposal/special-waste/Pages/default.aspx)
>* [Bureau of Design and Environment Manual](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf)
>* [Bureau of Local Roads and Streets Manual](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Local-Roads-and-Streets/Local%20Roads%20and%20Streets%20Manual.pdf)

