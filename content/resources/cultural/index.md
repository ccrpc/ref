---
title: "Cultural Resources"
draft: false
weight: 30
bannerHeading: Cultural Resources
bannerText: >
  Champaign County was the original home of the Kickapoo Tribe prior to Euroamerican settlement and land conversion from the 1800’s through today.  The area is rich in cultural history from its residents throughout the centuries.
---


{{<image src="cultural.png"
  alt="Figure showing Cultural Resources within Champaign County"
  attr="Cultural Resources" attrlink=""
  position="full">}}
  

## Historic Bridges
The IDOT environmental review process requires that planners identify historic
bridges in the proposed project area as early as practical during planning.
Department review and approval is required if a project involves the replacement
or rehabilitation of a bridge 50 years or older [(IDOT, 2017)](http://apps.dot.illinois.gov/environment/onlineforms.asp). Three historic
bridges exist within the County, only two of which are listed on IDOT’s historic
bridges resource ([Historic Bridges, n.d.](
https://historicbridges.org/b_a_list.php?ct=&c=&ptype=county&pname=Champaign+County,+Illinois)).
Both listed bridges (Hazen and East Street) are located in the Village of
Mahomet.  The third historic bridge, Old Stone Arch Bridge, is listed on the
National Register of Historic Places, but not on the IDOT resource and is located
at Scott Park in Champaign.

<rpc-table
url="bridges.csv"
table-title="Champaign County Historic Bridges"
source="National Park Service (2020)"
source-url="">
</rpc-table>

## Historic Places
In transportation planning it is necessary to consider potential impacts to
properties included in the National Register of Historic Places (NRHP).  The
[NRHP]( https://www.nps.gov/subjects/nationalregister/database-research.htm)
lists 59 historic places in Champaign County as of 2020, including historically
significant sites, districts, buildings, structures, and objects. Most of these
are located in Champaign and Urbana, but Mahomet and Rantoul each have one,
respectively. While 59 places have been listed on the NRHP, many more sites
still remain eligible, yet unlisted.  

Section 106 of the National Historic Preservation Act of 1966 establishes
protection and maintenance procedures for sites listed or eligible for listing
in the NRHP.  [Section 106](https://www.youtube.com/watch?v=0ayArv5ZTAc)
requires impacts and alternatives to impacts be considered during project
planning.  Agreements must also be reached amongst local agencies, consulting
agencies, IDOT, State Historic Preservation Office (SHPO), and have public
comment before further actions take place.

Section 4(f) of the Department of Transportation Act of 1966 also applies
protection to historic sites.  A [Section 4(f)
Evaluation](https://www.environment.fhwa.dot.gov/env_topics/4f_tutorial/default.aspx)
is a Federal requirement that involves acquiring a permit from an agency of the
US DOT should a transportation project petition use of any land from a historic
site of national, state, or local significance.  Planners must consider any
potential impacts, develop alternatives, document measures to minimize any harm,
and net benefits that would result from use of the land.

An IDOT [Environmental Survey Request](http://apps.dot.illinois.gov/environment/onlineforms.asp) (ESR) will trigger project review if a project involves a historic structure, is within the
limits of a historic district, and/or the project potentially affects a historic
district or property listed on the NRHP.  Throughout all project planning, it is
crucial that planners coordinate with all state and local historic preservation
agencies.  In Champaign County those include:
- [The State Historic Preservation Office](https://www2.illinois.gov/dnrhistoric/Pages/default.aspx) (SHPO). Also known as the Illinois Historic Preservation Agency (IHPA).
- [Champaign County Historical Society](https://www.champaigncountyhistoricalmuseum.org/)
- [Urbana Historic Preservation Commission](https://www.urbanaillinois.us/boards/historic-preservation-commission)
- [Champaign Historic Preservation Commission](https://champaignil.gov/boards_inside/historic-preservation-commission/)

The cities of Urbana and Champaign both have historic preservation ordinances.
In many cases, a Certificate of Appropriateness is required by the municipality
to approve of alterations of either a designated landmark or a structure within
a designated historic district. These ordinances can be found here:
- [Champaign Code of Ordinances. Ch 37. Article IX - Historic Preservation](https://library.municode.com/il/champaign/codes/code_of_ordinances?nodeId=MUCO_CH37ZO_ARTIXHIPR). Review criteria found at Sec. 37-522.
- [Urbana Zoning Ordinance. Article XII - Historic Preservation](https://urbanaillinois.us/zoning#:~:text=The%20Urbana%20Zoning%20Ordinance%20is,outlined%20for%20each%20zoning%20district). Review criteria found at Section XII-6.C

## Historic Districts 
A subset of historic places, historic districts designate entire areas with
significant historic resources in a community.  These often include central
business districts, residential neighborhoods, industrial areas, rural areas,
and could apply to entire communities ([GDNR, n.d.](https://gadnr.org/sites/default/files/hpd/pdf/NR%20Historic%20District%20vs.%20Local%20Historic%20District.pdf)).  Districts listed in the
National Register are mainly used to identify historic areas for planning and
development purposes.  Some protection is applied to designated districts but
uses of private property by owners are not restricted in any way.  More
stringent protections are available for local historic districts, which require
alteration review and approval through a Certificate of Appropriateness by
either the Urbana or Champaign Historic Preservation Commission and the Zoning
Administrator.  The [Champaign Code of Ordinances](https://library.municode.com/il/champaign/codes/code_of_ordinances?nodeId=MUCO_CH37ZO_ARTIXHIPR), and [Urbana Zoning Ordinance](http://urbanaillinois.us/sites/default/files/attachments/2018-zoning-ordinance-linked.pdf)
should be consulted for local preservation guidelines.  

The IDOT ESR requires that National Register Districts within a project area be identified, as part of the compliance process.  Projects on properties adjacent to National Register Districts will also trigger review by the IDOT ESR even if there is no overlap with the District boundary.
<rpc-table
url="district.csv"
table-title="Champaign County National Register Districts"
source="NPS, 2020"
source-url="">
</rpc-table>

## Cemeteries
Champaign County is home to a total of 75 cemeteries spread out in relative
uniformity across the region. None of these cemeteries are listed on the NRHP,
but the National Environmental Policy Act (NEPA) specifies that sites with the
potential to be listed are protected, as well.  This can include cemeteries.
Planners need to document any identified cemeteries within the project area as
part of compliance with all Federal, state, and local cemetery protections. 

The Illinois Human Skeletal Remains Protection Act prohibits disturbances to
grave sites from any project without a permit from the Illinois Historic
Preservation Agency (IHPA). This [law](https://efotg.sc.egov.usda.gov/references/public/IL/IL_Laws_on_Historic_Preservation.pdf) also pertains to grave markers within
unregistered cemeteries over 100 years old on both private and public land.
Avoidance procedures must be considered before any work is done that may impact
a cemetery. If avoidance is impossible, an application for grave and remains
removal must be submitted to the SHPO describing the alternatives considered and
the reasons the burial site cannot be avoided.

## Archaeological Area
At the Federal-level, protection of archaeological resources falls under the
NEPA umbrella, meaning NEPA-compliance facilitates compliance with other
legislation regarding archaeological resources.  Most compliance can be
satisfied by reviewing Section 106 of the NHPA, which requires impacts and
alternatives to impacts be considered during project planning.  Further
Agreements must also be reached amongst local agencies, consulting agencies,
IDOT, SHPO, and have public comment before further actions take place.

The IDOT ESR will trigger a site investigation when archaeological resources are
found on a project site.  IDOT review procedures will then mirror those of
Section 106.  Other state protections of archaeological resources exist, but
usually follow the same process.  A detailed list of preservation laws can be
found [here](https://www2.illinois.gov/dnrhistoric/About/Pages/Laws.aspx).  These include:
- [Illinois State Agency Historic Resources Preservation Act](https://www.ilga.gov/legislation/ilcs/ilcs3.asp?ActID=372&ChapterID=5)
- [Archaeological and Paleontological Resources Protection Act](https://www.ilga.gov/legislation/ilcs/ilcs3.asp?ActID=375&ChapterID=5)
- [Human Skeletal Remains Protection Act](https://www.ilga.gov/legislation/ilcs/ilcs3.asp?ActID=376&ChapAct=20%26nbsp%3bILCS%26nbsp%3b3440/&ChapterID=5&ChapterName=EXECUTIVE+BRANCH&ActName=Human+Skeletal+Remains+Protection+Act.)

SHPO has the responsibility for protection of areas with a high probability of
containing archaeological sites, sites that contain artifacts, or structures
linking to early human settlement or prehistory through review and
permit issuing.  In Champaign County, 300 yards from the bluff line crest
(valley wall) of all streams and rivers are considered to be potentially
archaeologically significant. However, a site investigation would be necessary
to determine whether regulatory protections apply. At all levels of protection,
planners must coordinate with SHPO when a project may impact a potentially
significant archaeological site ([LRMP, 2010](https://ccrpc.org/wp-content/uploads/2010/04/10_v1_Chapter8.pdf)).  More detailed information on
Illinois archaeological protection can be found on the SHPO site, [here](https://www2.illinois.gov/dnrhistoric/Preserve/Pages/Archaeology.aspx). 

The cities of Urbana and Champaign both have historic preservation ordinances that
mention archaeological areas.  Both ordinances use almost the exact same
language: “If archaeological resources must be disturbed, then mitigation
measures shall be undertaken” ([City of Champaign, MC Sec. 37-523](https://ccrpc.org/wp-content/uploads/2010/04/10_v1_Chapter8.pdf)).  These
ordinances can be found here: 
- [Champaign Code of Ordinances. Ch 37. Article IX. Sec 37-523](https://library.municode.com/il/champaign/codes/code_of_ordinances?nodeId=MUCO_CH37ZO_ARTIXHIPR) 
- [Urbana Zoning Ordinance, Article XII-6](http://urbanaillinois.us/sites/default/files/attachments/2018-zoning-ordinance-linked.pdf)

