---
title: "Topography & Soil"
draft: false
weight: 10
bannerHeading: Topography & Soil
bannerText: >
  Champaign County’s highest elevation is approx. 860 feet above sea-level, and it’s lowest is approx. 620 feet above sea-level, resulting in a relief of 246 feet.
---

## Topography
Champaign County is known for its flat, expansive agricultural character. Over
its 998 square mile area, Champaign County is one of the flattest areas in
Illinois with elevations ranging from approximately 262 meters (860 feet) above
mean sea level near the north of Rising Township to 189 meters (620 feet) above
mean sea level near the Salt Fork River in Homer Township. The relief, highest
elevation minus the lowest, is only 75m (246 feet). Relief effects drainage,
runoff, erosion, and deposition. Topography is important to consider when
looking at flood potential and foundation stabilization of a built project.
Areas with high relief indicate an uneven topography that can cost developers
large amounts to grade and make suitable for building.  Areas of low elevation
tend to be in riparian zones along rivers and near bodies of standing water.
These areas are more likely to be within a floodplain and/or have hydric soils
present.  

{{<image src="topo.png"
  alt="Figure showing Topography of Champaign County"
  attr="Topography" attrlink="https://apps.nationalmap.gov/downloader/"
  position="full">}}

Planners should be sure to note the flooding potential, soil quality, and relief
of an area to help develop project alternatives that avoid working within
unfavorable conditions.  Planning projects that follow topographic contour lines
helps minimize unnecessary cutting and filling of soil, as well as increase
stability of foundations.  As topography is a strong indicator of unfavorable
conditions, consult the topographic maps of a project area closely and document
potential issues that could arise.

## Soil
Champaign County is comprised of 75 different soil types. The most prevalent
soil type is drummer silty clay loam with 0 to 2 percent slope, which covers 40
percent of the County. This is a hydric soil, meaning that it is saturated with
water for part or all of the year and has very limited development potential.
These heavily saturated substrates can pose significant issues for
infrastructure development, so it is crucial to consult soil maps to identify
where potential planning issues may arise ([NRCS, 1998](https://www.nrcs.usda.gov/Internet/FSE_MANUSCRIPTS/illinois/IL019/0/champaign_IL.pdf)).

*[Hydric soils](https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/ref/?cid=nrcseprd1343020): formed under conditions of saturation, flooding, or ponding long enough during the growing season to develop anaerobic conditions within the top 20 inches of soil depth.* 
- Limited infiltration of water resulting from oversaturated pores that can cause surface ponding, flooding, weak structure, and are highly erodible.
- Very limited development potential
- Most common in low-lying areas, wetlands, and near waterbodies

The role of soil in urban planning focuses mainly on the loss of ecosystem services, particularly:
- Loss of productive agricultural land: With 90 percent of Champaign County land
  in agricultural production, soil sustainability should be incorporated into
  projects to comply with the [Farmland Protection Policy Act](https://www.nrcs.usda.gov/wps/portal/nrcs/detail/national/landuse/fppa/?cid=nrcs143_008275) and reduce
  conversion of regionally important agricultural land.  Using the Interactive
  Map, planners should avoid conversion of farmland to non-agricultural uses to
  limit irreversible damage and avoid potential regulatory penalties.  

- Loss of water management: Healthy soil allows for flooding and pollution
  control while also providing area for the recharge of aquifers, like the
  Mahomet Aquifer.  Development with impervious pavement degrades soils natural
  infiltration and evapotranspiration through compaction, and erosion (Figure
  3). Planners should use the REF, in conjunction with the [Champaign County Soil Survey](https://websoilsurvey.nrcs.usda.gov/app/), to identify the soil conditions of a project site and plan for their sustainable use.

{{<image src="water.jpg"
  alt="Figure showing Effects of development on water management"
  attr="Effects of development on water management (US EPA, 2003)" attrlink="https://twitter.com/therantoulpress/status/1067481142369107968"
  position="right">}}

- Reduced development potential: Champaign County soils fall under three classes
  based on their development potential for different building types to overcome
  soil limitations. The REF considers development potential of small commercial
  buildings, as it is the most appropriate to transportation projects ([NRCS, 2019](http://www.ccswcd.com/MapsData/)).  The [classes](https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/ref/?cid=nrcseprd1343020) are: Not Limited, Somewhat Limited, and Very Limited.

- Common development potential [limitations](https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/ref/?cid=nrcseprd1343020) include:
>* Ponding: Buildup of surface water through improper drainage
>* Depth to Saturated Zone: Depth from surface to where all soil pores are filled with water.
>* Shrink-swell: Extent to which soil will expand when wet and retract when dry; also referred to as linear extensibility or expansive soil.
>* Flooding: Over saturation of water.
>* Slope: Angle of incline or decline, expressed in the percent of rise or fall of the soil surface from the horizontal over a distance of 100 feet. Slope affects the surface water movement that can cause erosion, drainage, and stability issues.

{{<image src="roadway.jpg"
  alt="Figure showing Shrink-swell effects on roadways"
  attr="Shrink-swell effects on roadways (Mintek Resources, 2021)" attrlink=""
  position="right">}}

Both Urbana and Champaign have erosion control ordinances that often require
projects to have an Erosion and Sediment Control Plan (ESCP).  Acceptable best
management practices (BMPs) for erosion control can be found in the Cities of
[Urbana](https://www.urbanaillinois.us/sites/default/files/attachments/class-2-manual-of-practice_0.pdf) and Champaign Manuals of Practice and should be considered early in planning.  BMPs include but are not limited to: Vegetative Buffer Strips, Erosion Control Blankets, Detention Basins, and Sodding/Mulching.

{{<image src="soil.png"
  alt="Figure showing Soil Association Map of Champaign County"
  attr="Soil" attrlink="https://websoilsurvey.sc.egov.usda.gov/App/WebSoilSurvey.aspx"
  position="full">}}

## Agricultural Land
Agricultural land quality is determined through the [Champaign County Land
Evaluation and Site Assessment system](https://ccrpc.org/documents/land-evaluation-and-site-assessment-system-update/) (LESA), based on soil properties and site
characteristics.  The higher the LESA score, the more valuable the land is to be
protected for continued agricultural use.  The REF scores farmland using this
system, the breakdown for which is:
- Best Prime Farmland: “Prime Farmland Soils identified in the Champaign County
  LESA System that under optimum management have 91% to 100% of the highest soil
  productivities in Champaign County, on average”, as reported in the [Bulletin 811 Optimum Crop Productivity Ratings for Illinois Soils.](https://www.ideals.illinois.edu/handle/2142/1027)
- Prime Farmland: “Land that has the best combination of physical and chemical
  characteristics for producing food, feed, forage, fiber, and oilseed crops and
  is available for these uses” ([NRCS, n.d.](https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/ref/?cid=nrcseprd1343020)).
- Prime If… : Land that would qualify as Prime Farmland if a hazard or
limitation is overcome. i) Prime if drained. ii) Prime if protected from flooding
or not frequently flooded during the growing season. iii) Prime if drained and
either protected from flooding or not frequently flooded during the growing
season.
- Farmland of Statewide Importance: “Lands that are nearly Prime Farmland and
  that economically produce high yields of crops when treated and managed
  according to acceptable farming conditions” ([IDOA, 2001](https://www2.illinois.gov/sites/agr/Resources/LandWater/Documents/LESA.pdf)).   
- Not Prime Farmland: Agricultural land that does not meet the qualifications
  for Prime Farmland or Farmland of Statewide Importance.

Best Prime Farmland and Prime Farmland cover 93 percent of Champaign County.
The IDOT [Bureau of Design and Environment Manual](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf) Ch 24. Environmental Assessments
specifically includes Prime and Best Prime Farmland as resources of concern
within a project area.  Overall, planners must consider strategies to minimize
disturbance, first and foremost by using the REF Interactive Map to identify and
avoid these areas, as best as possible.

Other local regulations have been drafted to protect farmland soils for their national security, economic, and ecological value.  Compliance with these policies and regulations generally requires review of government-funded projects that convert agricultural land for non-agricultural purposes.  They include:
- [Champaign County Zoning Ordinance](https://www.co.champaign.il.us/planningandzoning/PDF/forms/Ordinance_Zoning.pdf) outlines lot area requirements and policies to minimize disturbance to Best Prime Farmland.
- [Storm Water Management & Erosion Control Ordinance](https://www.co.champaign.il.us/planningandzoning/PDF/forms/Ordinance_Storm_Water_Management_Erosion_Control.pdf) presents a hierarchy of best management practices for preserving Best Prime Farmland.