---
title: "Resources"
draft: false
menu: main
weight: 30
bannerHeading: Resources
bannerText: >
  Environmental and regulatory-significant resources are considered, as well as their connection to sustainable transportation planning.
---

This section includes key information on topography, soil, hydrology, cultural resources, vegetation and wildlife habitat, regulated-waste sites, and the ambient environment. Data is updated regularly and is current as of 2022. 