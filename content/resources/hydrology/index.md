---
title: "Hydrology"
draft: false
weight: 20
bannerHeading: Hydrology
bannerText: >
  Champaign County has over 1,300 miles of waterways, including 100 and 500-year floodplains, wetlands, biologically diverse streams, impaired streams, and a sole source aquifer (Mahomet Aquifer).
---

## Major Waterways
Champaign County waterways can be perennial, where water is consistently flowing
year-round, or intermittent, where water only flows at certain times of the year
([EPA, 2013](https://archive.epa.gov/water/archive/web/html/streams.html)).
Major waterways include the Sangamon River, Kaskaskia River, Embarras River,
Salt Fork River split, and the Middle Fork River.

{{<image src="Sangamon.webp"
  alt="Figure showing Sangamon River"
  attr="Sangamon River (Britannica, 2008)" attrlink="https://www.britannica.com/place/Sangamon-River"
  position="right">}}

Impacts to waterways, both perennial and intermittent, can pose serious
challenges planners need to consider.  Direct impacts of transportation projects
include pollution and habitat destruction from road construction and maintenance
actions, like dredging and chemical/debris runoff from impervious surfaces.
Indirect impacts, such as stream bank alteration, can disrupt flooding patterns,
resulting in more severe and unpredictable flood events. Additionally, expansion
of transportation systems often means more automobiles on the road, resulting in
more air pollutants that degrade water quality.

{{<image src="runoff.png"
  alt="Figure showing Road Maintenance Runoff Pollution"
  attr="Road Maintenance Runoff Pollution (Joshua Patton, 2021)" attrlink="https://abc3340.com/news/abc-3340-news-iteam/homeowners-complain-construction-runoff-flooding-and-damaging-their-property"
  position="right">}}

In general, planners should avoid development around waterways whenever possible.  However, if avoidance alternatives prove to not be reasonable, feasible, prudent, or practicable (NEPA litmus test for acceptable alternatives), then several regulatory issues require consideration:
- A [Section 404 permit](https://www.epa.gov/cwa-404/permit-program-under-cwa-section-404) must be obtained from the U.S. Army Corp of Engineers before discharging into waters of the United States.
- A [Creekway Permit](https://urbanaillinois.us/businesses/planning-zoning-forms/boneyard-creek-forms) is required for all new construction within the [Boneyard Creek District](https://www.urbanaillinois.us/zoning/boneyard_creek_district#:~:text=The%20general%20goals%20of%20the,that%20negatively%20affect%20water%20runoff.) to ensure development does not increase flooding risks or impact cultural, economic, and environmental quality of the creek for the city.
- The Champaign County Storm Water Management and Erosion Control [Ordinance Section 6](http://www.co.champaign.il.us/planningandzoning/PDF/forms/Ordinance_Storm_Water_Management_Erosion_Control.pdf) lists local requirements for protecting existing drainage and water resources that new projects must adhere to.
- General NEPA and IDOT procedures require that alternatives to potential adverse impacts to waterways be considered and adopted, when practicable. 

{{<image src="water_flood.png"
  alt="Figure showing waterways and flood zone"
  attr="" attrlink=""
  position="center">}}

## Flood Hazard Areas
Floodplains are the lowland and relatively flat areas adjoining waterways
subject to a one percent or greater chance of flooding in a given year. They
absorb and moderate the flow of significant amounts of water from flooding
events in adjacent waterways. Two categories of floodplains exist based on the
percent annual chance of flooding ([IDNR, 2001](http://dnr.state.il.us/waterresources)):
- 100-Year Floodplain: Also known as the “base floodplain,” this is the area
  subject to a one percent annual chance of flooding (1-in-100). Potential
  project sites within these areas will have issues with flooding. Insurance for
  such projects will be costlier, and steps must be taken to avoid negative
  impacts
- 500-Year Floodplain: Appearing on the fringes of the 100-year floodplain, this
  is the area subject to a 0.2 percent chance of flooding each year (1-in-500).
  While development is less limited than the 100-year floodplain, flooding
  issues within the 500-year floodplain are not uncommon.

When projects encroach on the floodplain, either longitudinal or transverse, the floodplain function is altered, often leading to increased flood damages, unpredictability in future flooding events, and habitat loss. Planners should avoid floodplain development whenever possible and should be aware of the following guidelines:

{{<image src="floodplain.jpg"
  alt="Figure showing Road through Floodplain in Clinton, IL"
  attr="Road through Floodplain in Clinton, IL (Herald & Review, 2019)" attrlink="https://herald-review.com/news/local/public_safety/heavy-rain-washes-out-roads-throughout-central-illinois-flood-warning-in-effect/article_ed936c96-6b2c-576c-ad0f-69af53abefd8.html"
  position="right">}}

- [Executive Order 11988](https://www.fema.gov/glossary/executive-order-11988-floodplain-management): Floodplain Management requires that alternatives to potential adverse impacts to floodplains be considered and adopted, when practicable. 
- The Champaign County [Special Flood Hazard Areas Ordinance](http://www.co.champaign.il.us/planningandzoning/PDF/forms/Ordinance_Special_Flood_Hazard_Areas.pdf) lists rules and guidelines concerning development in and around floodplains, including the [Floodplain Development Permit](https://www2.illinois.gov/dnr/WaterResources/Documents/Resman_ILFPMQuickGuide.pdf) that must be acquired before any person, firm, corporation, or governmental body not exempted by law before beginning development within the floodplain.
- [Creekway Permit](https://urbanaillinois.us/businesses/planning-zoning-forms/boneyard-creek-forms): all new construction within the [Boneyard Creek District](https://www.urbanaillinois.us/zoning/boneyard_creek_district#:~:text=The%20general%20goals%20of%20the,that%20negatively%20affect%20water%20runoff.) must have this permit to ensure development does not increase flooding risks or impact cultural, economic, and environmental quality of the creek for the city.

## Biologically Diverse Streams
In Champaign County 31 stream segments are considered Biologically Diverse
Streams (BDS). This designation is part of the [Biologically Significant Stream designation](https://www2.illinois.gov/dnr/conservation/BiologicalStreamratings/Documents/faqs_revised.pdf) developed by the Illinois Department of Natural Resources (IDNR) and
denotes streams of uniquely high biodiversity. The BDS designation uses fish,
macroinvertebrate (including sensitive populations), mussel, crayfish, and
threatened and endangered species data. The primary variable of a BDS is species
richness, or the number of different species represented in an ecological
community. Streams are graded A-E in terms of individual diversity scores and
the distribution of diversity scores found throughout an area, with A being the
highest diversity grade possible. These streams are located throughout the
County, with the northwest portion of the County having the highest rated stream
segments, along the Sangamon River. 

{{<image src="sample.jfif"
  alt="Figure showing Biological stream sampling"
  attr="Biological stream sampling (IDNR, 2008)" attrlink="https://www2.illinois.gov/dnr/conservation/BiologicalStreamratings/Documents/StreamRatingReportSept2008.pdf"
  position="right">}}

IDNR uses these stream ratings as the basis for resource protection and
implementation of the [Illinois Wildlife Action Plan](https://idnr.maps.arcgis.com/apps/MapJournal/index.html?appid=cbddf6b4a2574a569d28a268b9909823).  Planners should use BDS
ratings as a mechanism for identifying high-quality stream communities and
guiding planning decisions. When developing project alternatives, protection of
these streams should be prioritized.

{{<image src="diversity.png"
  alt="Figure showing Biologically Diverse Streams"
  attr="Biologically Diverse Streams" attrlink="https://www2.illinois.gov/dnr/conservation/BiologicalStreamratings/pages/default.aspx"
  position="center">}}

## Watersheds
Watersheds are the area of land that drains to one body of water.  They affect
the quality of the body of water they surround, as well as provide a host of
ecosystem services including nutrient cycling, carbon storage, erosion/flood
control, and water filtration ([EPA, 2020](https://www.epa.gov/waterdata/watershed-index-online)). 

Watersheds are identified by their hydrological unit code (HUC), separating
watersheds into six classes devised by the U.S. Geological Survey.  The HUC
describes the way smaller watersheds drain areas that together form larger
watersheds ([EPA, 2020](https://www.epa.gov/waterdata/watershed-index-online)). 

<rpc-table
url="huc.csv"
table-title="HUC Classification System"
source="NRCS Hydrologic Unit Codes 2007"
source-url="">
</rpc-table>

Generally, watersheds are named for the waterbody into which they drain.  Five
major watersheds (HUC-8) are found within Champaign County: Upper Sangamon,
Upper Kaskaskia, Vermilion-Wabash, Embarras, and Middle Wabash-Little Vermilion.

{{<image src="Watershed.png"
  alt="Figure showing Watersheds in Champaign County"
  attr="Watersheds" attrlink="https://apps.nationalmap.gov/downloader/"
  position="center">}}

Transportation infrastructure should be developed within the context of regional
management goals for natural resources and the environment.  Controlling and
managing land use is an important tool to preserving watershed health and
meeting LRTP goals.  Planners should review current zoning and/or projected
future land use to determine if a specific development project fits within
regional goals.  Planners also need to consider the impacts: (direct/indirect;
short/long term) to development within watersheds. Examples of these impacts
include impervious cover, drainage patterns, and habitat loss. The effect is to
shift development away from the stream and other water resources most impacted
by development and toward areas with lesser impact. Some basic watershed
planning goals that can guide project development include flood control, meeting
state water quality standards/designated use, wildlife habitat enhancement, and
greenway establishment

## Wetlands
IDOT defines wetlands as those areas that have hydric soils, inundation or
saturation by surface or groundwater, and prevalence of hydrophytic vegetation
([IDOT, 2019](http://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Split/Design-And-Environment/BDEManual/Chapter26SpecialEnvironmentalAnalyses.pdf)).  Wetlands provide a host of ecosystem services crucial to
economic, social, and ecological well-being.  Their continued function is
dependent upon planning efforts to reduce encroachment and minimize impacts from
transportation planning. The [American Planning Association Policy Guide on Wetlands](https://www.planning.org/policy/guides/adopted/wetlands.htm) states that the best way to protect wetlands is to identify them in
plans and develop alternatives to avoid adverse impacts.  To implement this
guidance, use the Interactive Map, in conjunction with the REF Strategies.

{{<image src="homer_lake.jpg"
  alt="Figure showing Homer Lake wetland"
  attr="Homer Lake wetland (CCFPD, n.d.)" attrlink="https://www.ccfpd.org/"
  position="right">}}

Champaign County has experienced a [40 to 60 percent](https://ccrpc.org/documents/active-choices-champaign-county-greenways-trails-plan-2014/#:~:text=The%20Champaign%20County%20Greenways%20%26%20Trails,to%20update%20the%202004%20plan.) loss in wetlands due to agricultural draining and land conversion since the 1800s.  The remaining wetlands have been seriously fragmented. Wetland protections require early incorporation into planning such as:
{{<image src="wetland.jpg"
  alt="Figure showing Draining of Midwest wetlands for agricultural land"
  attr="Draining of Midwest wetlands for agricultural land (The Wetlands Initiative, n.d)" attrlink="http://www.wetlands-initiative.org/how-the-midwest-lost-its-wetlands"
  position="right">}}

- [Section 404 permit](https://www.epa.gov/cwa-404/permit-program-under-cwa-section-404): must be obtained from the U.S. Army Corp of Engineers before discharging into waters of the United States, including wetlands.
- [Interagency Wetlands Policy Act](https://www.ilga.gov/legislation/ilcs/ilcs3.asp?ActID=279&ChapterID=5): With the goal of “no net loss” of wetlands throughout the state, IDNR has specific requirements on wetland compensation plans through mitigation banking.

{{<image src="wetlands.png"
  alt="Figure showing Wetland Types in Champaign County"
  attr="Wetlands Types" attrlink="https://www.fws.gov/program/national-wetlands-inventory/wetlands-data"
  position="center">}}

## Water Quality
Every two years, the Illinois EPA publishes a report on the quality of Illinois
surface and groundwater resources, the [Illinois Integrated Water Quality Report](https://www2.illinois.gov/epa/topics/waterquality/watershed-management/resource-assessments/Pages/default.aspx.).
Scientists determine water quality based on the ability of a waterway to support
several designated uses.  Waters not supporting any one of their designated uses
are deemed impaired and listed on the IEPA 303(d) List. The [303(d) List](https://www2.illinois.gov/epa/topics/water-quality/watershed-management/tmdls/Pages/303d-list.aspx)
inventories impaired streams, describes the causes and sources of impairment,
and gives priority rankings for strategies to meet water quality standards.
Priority rankings determine how urgently IEPA needs to set pollutant reduction
goals.  Each pollutant receives a target reduction, known as a Total Maximum
Daily Load (TMDL).  TMDLs determine permitting requirements for pollutant
discharge into waterways.  Approval times vary based on priority, but once
listed on 303(d), a stream segment must have a TMDL developed for it within 8-15
years.  The majority of Champaign County streams on the 303(d) List are of
“moderate” priority, meaning TMDL development will be closer to the 15-year
mark.  

{{<image src="water_quality.png"
  alt="Figure showing Water Quality Assessment in 2018"
  attr="Water Quality Assessment" attrlink="https://www2.illinois.gov/epa/topics/water-quality/watershed-management/tmdls/Pages/303d-list.aspx"
  position="right">}}

Currently, only three impaired stream segments in Champaign County have approved TMDLs:
- [Salt Fork Vermilion River](https://www2.illinois.gov/epa/Documents/epa.state.il.us/water/tmdl/report/salt-vermilion/salt-fork-vermilion-approved.pdf) (IL_BPJ-07)
- [Saline Branch Drainage Ditch](https://www2.illinois.gov/epa/topics/water-quality/watershed-management/tmdls/Documents/Saline_Stage1_PublilcDraft_060321.pdf) (IL_BPJC-08)
- [Boneyard Creek](https://www2.illinois.gov/epa/topics/water-quality/watershed-management/tmdls/Documents/Saline_Stage1_PublilcDraft_060321.pdf) (IL_BPJC-08) 

Regardless of whether or not a TMDL has been established for a listed stream in
a project area, it is prudent for planners to check if the associated watershed
has an approved TMDL.  Impacts to one segment have effects on another, and
segments should not be viewed as isolated systems.  Current Illinois TMDL
reports can be found [here](https://www2.illinois.gov/epa/topics/water-quality/watershed-management/tmdls/Pages/reports.aspx).

Champaign County waterways are evaluated for four designated uses: Aquatic Life,
Fish Consumption, Primary Contact (recreation), and Aesthetic Quality. Due to
limited resources, only about 15 percent of Illinois stream miles are assessed
for at least one designated use each reporting cycle.  Just over 40 percent of
Champaign County waterways were assessed in the 2018 Water Quality Report (575
miles out of 1,309).  Of those assessed, 148 miles have been listed on the
303(d) List, covering 12 stream segments across the County.  

{{<image src="impairement.jpg"
  alt="Figure showing Common Causes and Sources of Impairments in Champaign County (IEPA, 2018)"
  attr="Common Causes and Sources of Impairments in Champaign County (IEPA, 2018)" attrlink="https://www2.illinois.gov/epa/topics/water-quality/watershed-management/tmdls/Pages/303d-list.aspx"
  position="full">}}

As with all other resources, best practice is to avoid any impacts by locating
projects and alternatives away from vulnerable areas.  This may not always be
possible, however.  Planners need to know the status of potentially impacted
waterways in a project area. Answering the following questions, through use of
REF and IEPA resources, will result in fewer impacts and a more streamlined
environmental review/permitting process:

{{<image src="boneyard.jpg"
  alt="Figure showing Boneyard Creek Crossing"
  attr="Boneyard Creek Crossing (CCRPC)" attrlink=""
  position="right">}}

- Is the waterway 303(d) Listed?  If so, has a TMDL been established? 
- What are the limitations of the TMDL?  
- What causes and sources of impairment apply to this waterway?  
- How can further impairment be reduced, or quality improved with this project?  

## Mahomet Aquifer
At 3,700 sq. miles, the Mahomet Aquifer is one of the largest sand/gravel
aquifers in the state. The aquifer supplies 509,000 people across 14
counties with clean drinking water and is directly fed by the Sangamon River
([CCRPC, 2010](https://ccrpc.org/wp-content/uploads/2010/04/12_v1_Chapter10.pdf)).  The quality of the Sangamon River and those waterways connected
to the Sangamon all have an influence on what ends up in the Mahomet Aquifer.
Thus, Champaign County streams impact the drinking water for most of the County.

{{<image src="aquifer.jpg"
  alt="Figure showing Mahomet Aquifer and Champaign County"
  attr="Mahomet Aquifer and Champaign County" attrlink="https://champaignil.gov/public-works/find-a-service/mahomet-aquifer/f"
  position="right">}}

The Mahomet Aquifer is designated as a [Sole Source Aquifer (SSA)](https://www.epa.gov/dwssa/overview-drinking-water-sole-source-aquifer-program#What_Is_SSA) by the EPA.  As
an SSA, it supplies at least 50 percent of the drinking water for its service
area, and there are no reasonably available alternative drinking water sources
should the aquifer become contaminated.  This designation means that the EPA
conducts a separate review for projects 1) located within the area overlaying
the Aquifer and 2) receiving federal funding.  If a project has the potential to
contaminate the Aquifer, then the EPA will require modifications or federal
funding can be denied.  

Planners should be aware of the increasing stress put on the Aquifer by
urbanization.  Transportation projects should prioritize minimizing water usage
and potential contaminants from entering both streams and aquifer wells.
