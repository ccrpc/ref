---
title: "Wildlife & Vegetation Habitat"
draft: false
weight: 40
bannerHeading: Wildlife & Vegetation Habitat
bannerText: >
  Champaign County hosts a variety of natural areas, with varying degrees of human influence.  These areas provide for human needs as well as habitat for a diverse array of flora and fauna.
---

{{<image src="natural_areas.png"
  alt="Figure showing Natural Areas in Champaign County"
  attr="Natural Areas" attrlink=""
  position="full">}}

## Natural Areas
Champaign County has several types of natural areas, each with different rules
governing their use and management:
- Wooded areas consist of non-designated open space with prominent vegetative
  cover.  While not protected under any rules other than standard property
  rights, wooded areas provide habitat and ecological value.  

- Conservation Reserve Program (CRP) Land consists of environmentally sensitive
  land temporarily removed from agricultural production that is managed to
  improve environmental quality.  The USDA Farm Service Agency operates the [CRP](https://www.benefits.gov/benefit/340)
  through yearly rental payments to farmers in exchange for protection of the
  land.  No uses of any kind are authorized on CRP acreage during contract
  period, meaning transportation projects may not interfere with this land,
  including fencing or boundaries that prohibit wildlife access.

- Illinois Natural Area Inventory ([INAI](https://guides.library.illinois.edu/illinoisnaturepreserves/inai)) sites is an Illinois Department of
  Natural Resources (IDNR) designation that consists of high-quality natural
  areas, habitats of endangered species, and other significant natural features.
  No specific protections exist for INAI sites, but many have existing
  protections through other designations. For example, the Illinois Endangered
  Species Protection Act requires consultation for INAI sites due to their
  ecological sensitivity ([IDOT, 2012](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf)). Potential conversion of INAI sites often
  triggers agency or public resistance during comment periods.  As such,
  transportation project alternatives should avoid them as much as practicable.

- Conservation easements are properties with legally binding agreements between
  an owner and a local government agency, land trust, or other nonprofit
  organization that prohibits environmentally damaging land uses from occurring
  on a property.  New land uses of conservation easements must adhere to
  language in the legal agreements by the landowner and must not interfere with
  the conservation of natural resources on the land ([University of Illinois
  Extension, 2020](https://web.extension.illinois.edu/lcr/easement.cfm)).

- Illinois Nature Preserve Commission (INPC) protected areas are public and
  private lands dedicated to IDNR that contain rare plants, animals, or other
  unique natural features.  They are protected by the [Illinois Natural Areas
  Preservation Act](https://www2.illinois.gov/dnr/INPC/Pages/Defense.aspx) (INAPA), which prohibits actions that result in adverse
  effects.  Three types of [INPC](https://www2.illinois.gov/dnr/INPC/Pages/Protection.aspx) protected areas exist: 1) Illinois Nature
  Preserves, 2) Illinois Land and Water Reserves, and 3) Natural Heritage
  Landmarks.  Each type of area has varying strength of protections beyond those
  of the INAPA, including an [INPC Permit](https://www2.illinois.gov/sites/naturalheritage/permits/Pages/INPC-Sites.aspx) required for collection or research on
  a dedicated Nature Preserve or a registered Land and Water Reserve.

Planners must adopt conservation-oriented development standards in order to
avoid development of key natural areas and ensure long-term stewardship of
natural areas and open space, as outlined in the Champaign-Urbana Long Range
Transportation Plan 2045 [Environmental Strategies](https://ccrpc.gitlab.io/lrtp2045/goals/environment/).

## Threatened and Endangered Species
>Threatened Species: Likely to become endangered throughout its entire range.
>Endangered Species: On the brink of extinction throughout its entire range.

The [Endangered Species Act](https://www.epa.gov/endangered-species/about-endangered-species-protection-program) (ESA) requires that any federal action cannot jeopardize
a listed species or “destroy or adversely modify” critical habitat for listed
species. Specifically prohibited is the “take” of a threatened or endangered
species.  No project, transportation or otherwise, may involve a
“take” or “destroy or adversely modify” critical habitat.

<rpc-table
url="take.csv"
table-title="ESA “Take” Definition"
source="USFWS, n.d."
source-url="https://www.fws.gov/endangered/laws-policies/section-3.html">
</rpc-table>

{{<image src="species.png"
  alt="Figure showing Threatened or Endangered Species Observations (2020)"
  attr="Threatened or Endangered Species Observations" attrlink=""
  position="right">}}

Should a take be likely to occur, but not be the intention of an action, IDNRs
Endangered Species Protection Board in concert with the Illinois Nature Preserve
Commission and Division of Natural Heritage can issue an [Incidental Take Authorization](https://www2.illinois.gov/sites/naturalheritage/speciesconservation/Pages/Applying-for-an-Incidental-Take-Authorization.aspx).
Authorization requires a conservation plan including a description of the
impact, minimization and mitigation measures, alternative considerations, data
justifying the incidental take, and an implementing agreement. Criteria for
conservation plans can be found at [17 ILL. Adm. Code Ch. I, Sec. 1080](https://www2.illinois.gov/dnr/adrules/documents/17-1080.pdf).

Threatened and endangered species data comes from the [Illinois Natural Heritage Database Program](https://www2.illinois.gov/sites/naturalheritage/DataResearch/Pages/Access-Our-Data.aspx), a part of the Illinois Department of Natural Resources.  More data and information on a species or ecosystem including scientific and common names, conservation status, distribution maps, life history, conservation needs, and images can be found through the [NatureServe Explorer](https://explorer.natureserve.org/).

Using this website can help provide more in-depth explanations about species data and improve analysis on potentially impacted and regulatorily significant species in Champaign County.  Further information can be found through [Biotics 5](https://help.natureserve.org/biotics/biotics_help.htm), an integrated, web-enabled platform for tabular and spatial data management used by the NatureServe Network.

<rpc-table
url="list.csv"
table-title="2020 List of Endangered and Threatened Species in Champaign County"
source="IDNR (2020)"
source-url="">
</rpc-table>

## Parks and Recreation Areas
Champaign County has several types of park and recreation areas, each with different rules governing their use and management:
- Public/Private Recreational: Privately owned space whose use is recreation that may or may not be open to the public.  Examples include open space commons and clubs, such as the Champaign County Country Club and Curtis Orchard.
- Public Golf Course: Golf courses open to the public.  Examples include the University of Illinois Golf Course and Lake of the Woods Golf Course.
- Public Park: Publicly owned open space whose use is public recreation.  This is the most diverse type of park in the County, with a broad range of ecological values.  Examples include more recreationally geared areas like Thompson Park in Champaign, which hosts a playground, basketball court, and picnic area.  Other parks cater more to preserving natural quality, such as Weaver Park in Urbana, with its restored woodland, savanna, native grass, prairie, and wetland natural areas.
- Forest Preserve: Land set aside for its natural value and managed by the Champaign County Forest Preserve District.  Examples include River Bend Forest Preserve, and the Kickapoo Rail Trail corridor.

These classification types are not mutually exclusive with County natural areas,
meaning some parks may qualify for protections like the Illinois Natural Areas
Preservation Act (see Natural Areas).  In the case of grant funded sites,
transportation projects may not acquire these lands, except in the case where
the resulting land use is still public outdoor recreation or the maintenance or
expansion of recreational trails. 

Section 4(f) of the Department of Transportation Act of 1966 requires that
impacts to qualifying properties must be considered, avoided, or require further
analysis.  A [Section 4(f)](https://www.environment.fhwa.dot.gov/env_topics/4f_tutorial/default.aspx) property must be publicly owned, open to the public,
have its major purpose be for a park, recreation, or refuge activities, and it
must be significant.  Section 4(f) properties within a project area trigger an
IDOT environmental review and require approval of plans by the Federal Highway
Administration.  Planners should consult with IDOT as early as possible on
whether or not a park and recreation area qualifies for Section 4(f) protection.

## Grant Funded Sites
Several grant programs exist to help finance natural area, park, and open space projects that carry protections of their own.  Many of the sites previously listed in Natural Areas and Parks and Recreation Areas received funding from these types of grants.  It is important to distinguish between sites that have more ecological value, such as Weaver Park (restored prairie/wetland), from those that have little to none, like Canaday Park (mowed turf grass & basketball court).   To do this, staff has identified a group of grant programs, through research and Steering Committee input, to include as a separate EVL.  The grants chosen either have ecological-management encumbrances or are specifically for improving ecological quality of a site.  These grant programs are:
- [Land and Water Conservation Fund (LAWCON)](https://www2.illinois.gov/dnr/grants/Documents/2022%20LWCF%20Manual.pdf) course: Federal grant program providing matching funds for state and local parks and recreation projects, protection of working forests, city parks, wildlife habitat, critical drinking water supplies, and cultural resources.
- [Open Space Land and Acquisition (OSLAD)](https://www2.illinois.gov/dnr/grants/Pages/OpenSpaceLandsAquisitionDevelopment-Grant.aspx):  State-financed grant program providing funds for acquisition and/or development of land for public parks and open space.
- [Conservation 2000/Partners for Conservation (C2000/PFC)](https://www2.illinois.gov/dnr/conservation/pfc/pages/default.aspx): State program to fund ecosystem-based management of privately held land in a public-private partnership.
- [Illinois American Water - Environmental Grant](https://www.amwater.com/ilaw/news-community/environmental-grant-program/): Program to fund community-based environmental projects that improve, restore, or protect the watersheds, surface water and groundwater supplies in our local communities. 
- [Ameren - Right Tree Right Place](https://www.ameren.com/illinois/company/media/tree-planting):  Program that funds approaches to add trees or promote the principles of the Arbor Day Foundation’s [Tree City USA program](https://www.arborday.org/trees/righttreeandplace/)
- [Audubon Cooperative Sanctuary Program for Golf](https://auduboninternational.org/acsp-for-golf/) course: Golf courses certified by Audubon International as taking steps to enhance the valuable natural areas and wildlife habitats that golf courses provide, improve efficiency, and minimize potentially harmful impacts of golf course operations.

{{<image src="grant.png"
  alt="Figure showing Grant-Funded Sites in Champaign County"
  attr="Grant-Funded Sites in Champaign County" attrlink=""
  position="right">}}

Each program has different levels of requirements and protections for funded sites.  The highest protection comes from LAWCON, where sites must remain in perpetuity for public outdoor recreation.  OSLAD has similar protections, although they only last for the lifetime of the project, determined by IDNR.  Transportation projects may not acquire these lands, except in the case where the resulting land use is still public outdoor recreation or the maintenance or expansion of recreational trails. For more details consult the webpages linked to each grant program.  