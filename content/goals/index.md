---
title: "Goals & Objectives"
draft: false
menu: main
weight: 50
bannerHeading: Goals & Objectives
bannerText: >
  The REF provides resource-specific objectives, indicators, and strategies for planners to consider when undertaking new transportation projects.
---
__Regional Conservation and Mitigation Goal:__

*The REF will seek to foster and maintain conditions for productive harmony
between the built and natural environment by reducing environmental impacts of
transportation projects within Champaign County, expanding coordination between
planning agencies and resource agencies, and incorporating strategies to
maintain and improve the ecological function, integrity, and economic value of
Champaign County’s natural resources.*

The regional conservation and mitigation goal advances the stewardship component
of the overarching REF goal: “REF will provide comprehensive and centralized
information on environmental, social, and cultural resources to understand and
incorporate into the transportation planning process, strengthening
environmental stewardship and facilitating all levels of environmental reviews.” 

To compliment the regional conservation goal, [Table 9-1 in first phase report](https://ccrpc.org/wp-content/uploads/2020/11/Final-REF-Report-11.25.2020.pdf) includes
resource-specific objectives, indicators, and strategies for planners to
consider when undertaking new transportation projects. Not every
objective and strategy will be applicable to every project.  It is up to the
planners and other REF-users to determine how the different strategies could be
applied in different contexts to achieve sustainable project outcomes.  The
indicators presented help gauge how environmental resources can be measured,
helping agencies track an individual project’s progress toward each objective
over time.  These indicators will not be tracked within the REF but can be used
by agencies carrying out projects to more holistically consider environmental
stewardship. For instance, the objectives, indicators, and strategies will
inform the Project Priority Guidelines, used by CCRPC (in its role as the
Metropolitan Planning Organization) to evaluate and document consistency between
the local use of federal Surface Transportation Block Group Program funds and
federal and regional transportation goals.  Objectives, indicators, and
strategies are in no particular order.

REF strategies were developed in accordance with the following local, state, and
federal plans: CCRPC Long Range Transportation Plan: 2045, 2010 Land Resource
Management Plan, Regional Water Supply Framework for Champaign County and
East-Central Illinois, Champaign County Greenways and Trails Plan, Illinois
Wildlife Action Plan, IDOT Stormwater Management Plan, and FHWA Integrated
Approach to Sustainable Roadside Design and Restoration Plan.

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Topography "%}}
**Objectives:** 
1.  Reduce road and slope erosion for new and existing transportation projects within Champaign County

**Indicators:**
1. Slope erosion in grams of sediment collected downslope per roadway project
2. Acres disturbed by road construction per year
3. Number of vegetated roadsides planted per year	

**Conservation & Mitigation Strategies:**
1. Land Use Planning: Integrate land use planning into project planning through consideration of environmental context.
2. Road Design: Use terrain features such as natural benches, ridgetops, and lower-gradient slopes to minimize area of road disturbance.
3. Revegetation: Establish a dense vegetative cover (non-invasive) to reduce erosion and increase surface protection.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Soil "%}}
**Objectives:** 
1. Reduce road and slope erosion for new and existing transportation projects within Champaign County
2. Prevent loss of productive soil within new project areas
3. Maximize ecological potential of project-area soils

**Indicators:**
1. Slope erosion in grams of sediment collected downslope per roadway project
2. Abundance of non-invasive plant species per square yard

**Conservation & Mitigation Strategies:**
1. Temporary Cover: Establish plants (native or non-invasive) for seasonal cover of bare soil where no cover previously existed.
2. Conservation Cover: Establish a cover of native plant species on a project site, where applicable.
3. Conservation Tillage: Minimize the number of times soil is tilled or disturbed at a project site.
4. Buffer Strips: Establish plant buffer strip on land adjacent to waterway, road, or other land use type.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Agricultural Land "%}}
**Objectives:** 
1. Minimize fragmentation of County’s agricultural land base and conserve farmland, generally applying more stringent development standards on best prime farmland.
2. Continue to periodically review the Site Assessment portion of the LESA system for potential updates every 10 years.
3. Utilize, as may be feasible, tools that allow farmers to permanently preserve farmland.

**Indicators:**
1. Square mileage of farmland, specifically noting square mileage of best prime farmland
2. LESA documentation update

**Conservation & Mitigation Strategies:**
1. Utilize the REF in project planning and preliminary screening to avoid converting farmland for new transportation projects, particularly best prime farmland.
2. As part of scheduled REF updates, update the LESA system every 10 years.


{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Waterways "%}}
**Objectives:** 
1. Reduce road and slope erosion for new and existing transportation projects within Champaign County
2. Increase drainage potential of transportation infrastructure
3. Limit potential for transportation projects to negatively impact County waterways

**Indicators:**
1.  Slope erosion in grams of sediment collected downslope per roadway project
2. Number of waterways added to the 303d list biannually
3. Occurrences per month of roadside mowing

**Conservation & Mitigation Strategies:**
1. Erosion control measures: Design and plan roadsides, medians, and other open spaces to minimize erosion
2. Culvert drainage improvements: Plan and design culverts for increased drainage, when emptying into waterways.
3. Emphasize redevelopment over new development: Review new development projects near waterways or catchments to decide if an existing site can be redeveloped instead.
4. Identify and avoid vulnerable areas: Identify waterways within a project area and avoid any potential impacts through alternative development.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Flood Zones "%}}
**Objectives:** 
1. Curtail impacts of seasonal flooding
2. Lower environmental footprint of drainage infrastructure

**Indicators:**
1. Increase runoff detention area
2. Number of storm water trees installed
3. Number of redevelopment projects  

**Conservation & Mitigation Strategies:**
1. Grey Infrastructure: Built systems employed to collect runoff and discharge it quickly through the system.
2. Green Infrastructure: Methods that utilize the natural functions of the environment to reduce flooding and runoff, often at the source.
3. Planning Strategies: Growth and development framework to support local solutions to flooding.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Biological Stream Diversity "%}}
**Objectives:** 
1. Increase biodiversity of County waterways
2. Reduce road and slope erosion for new and existing transportation projects within Champaign County
3. Lower environmental footprint of new transportation projects

**Indicators:**
1. Abundance of aquatic species of conservation concern per square mile 
2. Abundance of invasive species per square mile 
3. Miles of Biologically Diverse Streams per testing cycle (irregular testing cycle)

**Conservation & Mitigation Strategies:**
1. Ecological restoration: Restoring a degraded ecosystem to a reference state or to a desired functional level.
2. Emphasize redevelopment over new development: Review new development projects near waterways or catchments to decide if an existing site can be redeveloped instead.
3. Erosion control measures: Design and plan roadsides, medians, and other open spaces to minimize erosion.
4. Reduce habitat fragmentation: If new development is necessary, consider alternatives to destroying portions of existing wildlife habitat.
5. Identify and avoid vulnerable sites:  Identify waterways within a project area and avoid any potential impacts through alternative development.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Water Quality & Watersheds "%}}
**Objectives:** 
1. Decrease pollution from point and non-point sources into County waterways and catchments
2. Reduce road and slope erosion for new and existing transportation projects within Champaign County
3. Promote efficient, natural drainage within Champaign County

**Indicators:**
1. Acreage of new development within 200 feet of waterways and catchments per year
2. Number of waterways added to the 303d list biannually
3. Number of TMDLs established biannually

**Conservation & Mitigation Strategies:**
1. Minimum setback zones: Establish a radius around waterways and catchments prohibiting any development that may impact water quality.
2. Sustainability Goals: Develop sustainability goals that account for project specific context.
3. Erosion control measures: Design and plan roadsides, medians, and other open spaces to minimize erosion.
4. Bioretention basins: Establish wetland vegetation basin in locations where run-off accumulates.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Aquifers "%}}
**Objectives:** 
1. Implement sustainable water-use practices within Champaign County
2. Decrease groundwater pollution from point and non-point sources within Champaign County

**Indicators:**
1. Gallons per day of water used
2. Number of water-saving appliances installed per year
3. PPM of nitrates in groundwater per testing cycle

**Conservation & Mitigation Strategies:**
1. Sustainable Landscaping: Promote use of native plants in landscaping.
2. Minimum setback zones: Establish distance around groundwater wells where there can be no development or land use that may impact the groundwater quality.
3. Retrofit water-saving appliances: Retrofit faucets and toilets with water-saving versions.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Wetlands "%}}
**Objectives:** 
1. Reduce wetland habitat losses within Champaign County
2. Increase abundance of wetland species in Champaign County

**Indicators:**
1. Wetland habitat per square mile
2. Biodiversity of wetland habitat per square mile

**Conservation & Mitigation Strategies:**
1. Ecological restoration: Restore a degraded ecosystem to a reference state or to a desired functional level.
2. Mitigation Banking: Pay for establishment or enhancement of an off-site wetland to compensate for impacts to on-site wetlands.  
3. Protection: Purchase wetland to designate as a Nature Preserve, Natural Area, or easement property.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Air Quality "%}}
**Objectives:** 
1. Maintain attainment status for 8-hour ozone levels for Champaign County each year between 2020 and 2025 (LRTP 2045)
2. Increase number of publicly available alternative fueling and charging stations by 15 percent by 2025 (LRTP 2045)
3. Increase the proportion of low and zero emission transit vehicles in MTD’s fleet to 100 percent by 2025 (LRTP 2045)

**Indicators:**
1. 8-hour ozone attainment levels per year
2. Number of alternative fueling stations
3. Percentage of MTD fleet’s low and zero emission transit vehicles

**Conservation & Mitigation Strategies:**
1. Develop cleaner travel options: Expand public transportation, improve public transportation service, and develop/improve bicycle and pedestrian infrastructure
2. Improve efficiency of land use planning and zoning: Reduce the distance between key destinations and encourage non-motorized vehicle travel.
3. Create or support clean fueling infrastructure: Expand electric vehicle charging and hydrogen fueling stations
4. Buy green fleet vehicles and equipment: Invest in hybrid and electric vehicles for County facilities

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Noise "%}}
**Objectives:** 
1. Maintain average peak-hour traffic noise below 80 decibels for all principal arterial roads within the County

**Indicators:**
1. Measurements 80+ decibels of peak-hour traffic noise per year

**Conservation & Mitigation Strategies:**
1. Noise Barriers: Construct noise barriers, including acquisition of property rights, either within or outside of the highway right-of-way.
2. Traffic management measures: Install traffic infrastructure to reduce traffic volume.
3. Roadway Alterations: Alter horizontal and vertical alignments of roadways.
4. Noise Buffer Zone: Acquire real property or interests therein to serve as a buffer zone to preempt development that would be adversely impacted by traffic noise.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Light "%}}
**Objectives:** 
1. Limit nighttime light pollution within Champaign County

**Indicators:**
1. Number of light-timing installations per year
2. Number of light guards installed per year

**Conservation & Mitigation Strategies:**
1. Light Orientation: Orient lights only downward or towards the target to minimize wasted light.
2. Light Technology: Use light-timing and smart technology to keep lights on when needed, and off when not.
3. Light Guards: Use light-guards to focus light and reduce external pollution (i.e. block the back of streetlamps to avoid polluting behind the streetlamp).
4. Alternative Light Sources: Avoid high intensity blue emission sources, like white LEDs.  These sources produce the most disruptive spectra of light to organisms. 

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Natural Areas "%}}
**Objectives:** 
1. Expand protected natural areas within Champaign County
2. Prevent losses in overall biodiversity within Champaign County

**Indicators:**
1. Abundance of species of conservation concern per square mile
2. Protected natural areas per square mile

**Conservation & Mitigation Strategies:**
1. Protection:  Purchase wetland to designate as a Nature Preserve, Natural Area, or easement property.
2. Ecological Restoration:  Restore a degraded ecosystem to a reference state or to a desired functional level.
3. Identify and avoid vulnerable sites:  Identify natural areas within a project area and avoid any potential impacts through alternative development.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Threatened & Endangered Species Habitat "%}}
**Objectives:** 
1. Increase the number of occurrences of T&E species within Champaign County
2. Increase acreage of continuous wildlife habitat within Champaign County

**Indicators:**
1. Number of reported occurrences of state listed species within Champaign County per year
2. Protected natural areas per square mile

**Conservation & Mitigation Strategies:**
1. Habitat Connectivity: Promote connectivity of existing T&E species habitat.
2. Reduce fragmentation: If new development is necessary, consider alternatives to destroying portions of existing wildlife habitat.
3. Identify and avoid vulnerable sites:  Identify T&E species habitat within a project area and avoid any potential impacts through alternative development.

{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Cultural Resources "%}}
**Objectives:** 
1. Preserve cultural amenities that provide high quality of life for citizens.
2. Reduce Section 106 Adverse Effect determinations
3. Increase member agency coordination with IDOT, Certified Local Governments, and the State Historic Preservation Office

**Indicators:**
1. Section 106 Adverse Effect determinations per year

**Conservation & Mitigation Strategies:**
1. Early identification: Identify cultural resources early in the planning process to design avoidance alternatives
2. Avoidance Alternatives: Design project alternatives to avoid impacting cultural resources


{{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title=" Special Waste "%}}
**Objectives:** 
1. Increase early identification of special waste sites within new project areas

**Indicators:**
1. Number of special waste sites identified during project planning

**Conservation & Mitigation Strategies:**
1. Early identification: Identify special waste sites early in the planning process to properly address cleanup or avoidance procedures

{{%/accordion-content%}}
{{</accordion>}}