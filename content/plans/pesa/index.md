---
title: "PESA"
draft: false
weight: 20
bannerHeading: PESA
bannerText: >
  Preliminary Environmental Site Assessments are required when a recognized environmental condition is identified in a project area.
---

Prior to acquisition of a right-of-way (ROW) or improvements to existing
state-owned property, IDOT must conduct a [Preliminary Environmental Site
Assessment
(PESA)](https://www.ideals.illinois.edu/bitstream/handle/2142/50120/c585.pdf?sequence=2).
The purpose of a PESA is to identify and assess environmental risks and
liabilities of a property to protect worker and public safety, reduce IDOT
liability, and minimize delays through efficient and cost-effective operating
procedures. 

Conducted by the Illinois State Geological Survey (ISGS) on behalf of IDOT, a
PESA is triggered when an Environmental Survey Request (ESR) identifies the
likelihood a Recognized Environmental Condition (REC), found in the [BLRS Manual
Ch.20-12(2)](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Local-Roads-and-Streets/Local%20Roads%20and%20Streets%20Manual.pdf),
or other natural hazard associated with a property or ROW. 

PESAs result in one of two findings[^1]:
- No REC Finding: If a PESA does not identify any RECs (other than de minimis)
  within the project area then no further action is necessary. New circumstances
  can trigger a reevaluation and are listed in the [BDE Manual
  Ch.23-3.03(c).](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf)

- REC Finding: If a PESA identifies a REC within the project area, then further
consultation is necessary. A Preliminary Site Investigation may be necessary,
which includes soil, sediment, and groundwater sampling. 

Planners can reduce the need for a PESA, minimizing project delay, by
identifying RECs or natural hazards and developing plans and alternatives that
avoid them. Early identification saves a project time and money, and through the
REF, planners can understand and take steps to avoid potentially hazardous
sites.


[^1]: IDOT [BDE Manual Chapter 27-3.03: Preliminary Environmental Site Assessment](http://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Split/Design-And-Environment/BDEManual/Chapter%2027%20Environmental%20Surveys.pdf)