---
title: "NEPA"
draft: false
weight: 40
bannerHeading: NEPA
bannerText: >
  The National Environmental Policy Act establishes a national policy to help prevent environmental impacts while meeting the needs of human health and welfare.
---

#### What is NEPA: 
The National environmental Policy Act (NEPA) establishes an environmental review
process for integrating environmental considerations into federal planning and
decision-making. The NEPA process for achieving such goals comes through a
thoroughly documented analysis of impacts and alternatives, consultation with
other resource and affected agencies, and a public comment period prior to any
project implementation.  The level of detail and documentation required for each
project depends on the potential significance of environmental impact resulting
from the project.  The process is summarized in the figure below.

{{<image src="nepa_process.jpg"
  alt="Figure describes NEPA process"
  attr="NEPA process options" attrlink="https://environment.transportation.org/education/practical-applications/nepa-process/nepa-process-overview/"
  position="full">}}

#### When does NEPA apply: 
NEPA applies to [“major Federal actions”,](https://www.ecfr.gov/current/title-40/chapter-V/subchapter-A/part-1508#se40.37.1508_118) projects that receive any federal funding or require federal decision-making, such as approvals or consultations.    
A project going through the NEPA process can be categorized one of three ways:
- *CE: Categorical Exclusion – No Significant Impact*
- *EA: Environmental Assessment – Significant Impact Unknown*
- *EIS: Environmental Impact Statement – Significant Impact Likely*

#### Major Players:
1.	Lead agency: Agency responsible for preparation of a NEPA environmental
analysis.  This is always the Federal Highway Administration (FHWA) or Federal
Transit Authority (FTA). 

2.	Applicant: Governmental unit requesting Federal
action.  Most often this will be IDOT or CCRPC. 

3.	Project Sponsor: Governmental unit or other entity that conducts some of the environmental
analysis activities on behalf of the applicant.  Most often this will be IDOT.

4.	Participating Agency: Governmental unit that has an interest in the proposed
project and has accepted an invitation by the applicant to participate in the
NEPA process. This could be any agency involved, such as the Champaign County
Forest Preserve District, Champaign-Urbana Metropolitan Transportation
Department, or even CCRPC. 

### NEPA & the REF
The NEPA process begins when the project sponsor notifies the lead agency about
the details of the project and the potential list of Federal approvals
anticipated to be necessary.  Local planning agencies and other organizations
will not be responsible for initiating NEPA but can help to provide preliminary
analysis and documentation that informs initial notification.  Other Federal,
State, or local government entities, such as CCRPC, may act as joint lead
agencies with invitation from the required lead agencies.  Joint lead agencies
assume the roles, responsibilities, and authority of a lead agency. 

Key components the REF can help with:
- Understanding the Affected Environment: Using the REF improves understanding of the environmental, social, and economic context of the affected environment by identifying resources, impacted neighborhoods, and susceptibility to impact.  Such information streamlines scoping, purpose and need refinement, and overall improves the level of detail in required documentation.
- Class of Action determination: Planners can use the REF Interactive Map and resource reports to identify potentially impacted environmental resources and review considerations to determine the appropriate class of action to pursue.
- Preliminary Screening of Alternatives: Document this process, as NEPA reviewers will be more likely to give agency deference if the agency can show early considerations like these.
  - Identifying Possible Alternatives: Alternatives (solutions) analysis devised in planning can carry over into NEPA alternative analysis and preliminary screening. Using the location of the project, identify environmental features potentially impacted. From there, design several alternatives based around avoiding the resources.
  - Eliminating Alternatives: Identify the project area for an alternative and what environmental features could be impacted from that alternative. Rank alternatives based on their potential for environmental impact and eliminate from consideration those that are most impactful or not reasonable, feasible, prudent, or practicable. 
