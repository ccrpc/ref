---
title: "Other Laws and Requirements"
draft: false
weight: 50
bannerHeading: Other Laws and Requirements
bannerText: >
  Reducing environmental impact and improving sustainability of transportation projects requires the holistic consideration of environmental laws and guidelines beyond IDOT and NEPA.
---

#### Safe, Accountable, Flexible, Efficient, Transportation Equity Act: A Legacy for Users [(SAFETEA-LU)](https://environment.transportation.org/pdf/map_21/safetea-lu2.pdf) 

SAFETEA-LU addresses a wide range of issues including new funding options for
highway and transit programs, streamlining of environmental reviews,
environmental planning provisions, and transportation planning provisions.
Sections 6002 and 6001 provide guidance on these issues.

#### Moving Ahead for Progress in the 21st Century Act [(MAP-21)](https://www.fhwa.dot.gov/map21/) 

MAP-21 expands project qualifications for a Categorical Exclusions, where no
full NEPA review is required, as well as removes requirements to analyze
alternatives as long as the preferred solution was previously analyzed during
the long-range transportation planning process.

#### [Section 4(f)](https://www.environment.fhwa.dot.gov/env_topics/4f_tutorial/default.aspx) of the Department of Transportation Act of 1966 

Sections 4(f) requires the consideration of park and recreation lands, wildlife
and waterfowl refuges, and historic sites during transportation project
development. Qualifying properties and projects must receive FHWA approval
showing that there is no feasible and prudent alternative and all possible
planning to minimize harm to the Section 4(f) property has been considered.
Section 4(f) properties are determined on a case-by-case basis, but the REF
contains many potentially qualifying property categories.

#### [Section 404](https://www.youtube.com/watch?v=jPBV8x985p0) of the Clean Water Act 

Section 404 is a permit program that regulates dredge or fill material
discharged into waters of the United States, including wetlands.  A Section 404
permit must be obtained from the U.S. Army Corp of Engineers before discharging
into waters of the United States, with certain exceptions.  Permit applicants
must show that to the extent practicable, steps have been taken to avoid wetland
impacts, potential impacts on wetlands have been minimized, and compensation for
unavoidable impacts has been made.

#### [Section 106](https://www.youtube.com/watch?v=0ayArv5ZTAc) of the National Historic Preservation Act of 1966 

Section 106 requires that Federally assisted projects must consider effects to
historic properties.  Section 106 properties are those listed or eligible for
listing in the National Register of Historic Places.  Properties must be
identified, and the effects assessed and resolved prior to construction.

#### [Executive Order 12898](https://www.transportation.gov/transportation-policy/environmental-justice/environmental-justice-strategy): Federal Actions to Address Environmental Justice in
Minority Population and Low-Income Populations 

Executive Order 12898 directs federal agencies to identify and address
environmental justice (EJ) issues.  EJ issues include the disproportionate
effects to human and environmental health felt by minority and low-income
populations.  The U.S. DOT developed an EJ strategy to determine likelihood of
disproportionate effects and to avoid, reduce, or mitigate those effects.  

#### [Executive Order 11988: Floodplain Management](https://www.fema.gov/floodplain-management) 

Executive Order 11988 requires that federal agencies adopt practicable
alternatives, when available to potential long and short-term adverse impacts to
floodplains.  Project that are within the 100-year floodplain must conduct a
public review and identify practicable alternatives to project locations.

#### Interagency Wetlands Policy Act of 1989 [(IWPA)](https://www.aswm.org/pdf_lib/state_summaries/illinois_state_wetland_program_summary_083115.pdf)

IWPA gives authority to the Illinois Dept. of Natural Resources to regulate
state-funded actions that impact state wetlands.  The IWPA has a stated goal of
“no net loss” of wetlands and includes a separate review process for projects
that could potentially impact a wetland.  IWPA sets wetland compensation plans
for projects with a likely impact.
