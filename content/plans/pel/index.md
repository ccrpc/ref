---
title: "PEL"
draft: false
weight: 30
bannerHeading: PEL
bannerText: >
  Planning and Environmental Linkages offer opportunities to facilitate environmental reviews and improve the understanding of environmental conditions and project alternatives.
---

Planning & Environment Linkages (PEL) represent a holistic and cooperative
approach to transportation decision-making that (1) considers environmental,
community, and economic goals early in the transportation planning process; and
(2) uses the information, analysis, and products developed during planning to
inform the environmental review process[^1].  IDOT guidance on appropriate
application of PEL can be found in the [BDE Manual Ch. 11.](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf) PEL planning products
help set the stage for future projects by improving the understanding of needs,
logical termini, and/or improvement alternatives.  

PEL Planning Products[^2][^3]
<rpc-table
url="pel.csv"
table-title="PEL Planning Products"
source=""
source-url="">
</rpc-table>


The benefits of PEL include, but are not limited to: 
- Minimize duplication of efforts
- Enhanced community involvement
- Decisions & analysis to inform NEPA
- Improved relationships & coordination
- Improved documentation

For more information on PEL, consult the [FHWA Environmental Review Toolkit.](https://www.environment.fhwa.dot.gov/env_initiatives/PEL.aspx)



[^1]: FHWA [PEL Fact Sheet](https://www.environment.fhwa.dot.gov/env_initiatives/pel/pel_fact_sheet.pdf) 
[^2]: 23 U.S.C. 168(c)(1)
[^3]: 23 U.S.C. 168(c)(2)
