---
title: "IDOT Environmental Surveys"
draft: false
weight: 10
bannerHeading: IDOT Environmental Surveys
bannerText: >
  Transportation projects within the region often utilize state funds or rights-of-way. When this occurs, Illinois Department of Transportation guidelines and requirements must be followed.
---

An Environmental Survey Request (ESR) is the first step in cultural, biological,
and special waste resource screening for a project. An ESR is essentially a
checklist of conditions that determine whether IDOT needs to further review
impacts of a transportation project. If any criteria on the form are met, then
an ESR is submitted by the planning agency.  Prior to any action on a project,
planners should consult the ESR form to determine if any conditions are met, and
to begin early considerations of alternatives that may not require an ESR or
other environmental documentation.

<rpc-table
url="ESR.csv"
table-title=" "
source="IDOT ESR, 2011"
source-url="http://apps.dot.illinois.gov/environment/onlineforms.asp">
</rpc-table>

Upon submitting the ESR, the Bureau of Design and Environment (BDE) will, as
needed, consult with the Illinois Dept. of Natural Resources (IDNR) to determine
if more field surveys are necessary. The field surveys are carried out by the
Illinois Natural History Survey (INHS) on behalf of the BDE. If the BDE
determines that no adverse effects (listed in BDE Manuel 27-1.03(b)) will occur
to the resources listed above, then the project will be given a biological
resource clearance, valid for two years. If the BDE determines that an adverse
effect will occur to any of the resources listed above, the BDE submits the
projects to IDNR. IDNR then will issue one of the following responses:  

- Adverse effects unlikely – consultation terminated 
- Additional information or biological survey requested 
- Minimization of adverse effects through IDNR recommended methods

INHS biological surveys may be required, along with alternative evaluations to
determine if adverse effects are avoidable given the IDNR recommendations and
field survey results. Further studies may be necessary, but the BDE will
determine next steps and coordinate with relevant agencies.  This process can be
visualized in figure below.

{{<image src="ESR_review_Process.jpg"
  alt="Figure describes relationship between mobility and access"
  attr="ESR Response Process" attrlink="https://ops.fhwa.dot.gov/access_mgmt/presentations/am_principles_intro/index.htm"
  position="full">}}

All IDOT environmental manuals and guides, including links to the Web-ESR and
Wetland Impact Evaluation forms can be found here: [IDOT Consultant Resources.](https://idot.illinois.gov/doing-business/procurements/engineering-architectural-professional-services/Consultants-Resources/index)
