---
title: "Plans & Guidelines Review"
draft: false
menu: main
weight: 20
bannerHeading: Plans & Guidelines Review
bannerText: >
  The REF was developed using a host of federal and state environmental review guidelines, requirements, and planning approaches relevant to transportation.
---
This section contains an overview of federal and state environmental review guidelines and requirements relevant to transportation planning.