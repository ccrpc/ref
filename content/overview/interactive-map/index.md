---
title: "Interactive Map"
draft: false
weight: 30
bannerHeading: Interactive Map
bannerText: >
  REF-users can visualize and analyze potential negative impacts of a transportation project with the tool like a spatial query, pop-up attribute table, and exportable map.
---

This Interactive Map is to be used as a preliminary screening tool for
identifying review-triggering resources and potentially ecologically vulnerable
areas within Champaign County. The map contains individual resource layers, as
well as a Composite Score of Ecological Vulnerability. Consult the User Guide
for detailed process for using the Map.

<iframe src="https://maps.ccrpc.org/ref-map-2022/"
  width="100%" height="500" allowfullscreen="true">
  Map showing the ecological, social, cultural and regulated substances in the region intended to strengthen environmental stewardship in the transportation planning process.
</iframe>

[Interactive Map User Guide](draft_user_guide.pdf)

[REF Data Dictionary](Acronym_Key.pdf)

[REF Acronym Key](Data_Dictionary.pdf)

### Interactive Map Tutorial
<iframe width="600" height="400" src="https://www.youtube.com/embed/OKYieVMC_xY" title="Interactive Map Tutorial" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

_This map/data was created by the Champaign County Regional Planning Commission (CCRPC) for use “as-is” and as an aid in graphic representation only. The data is not verified by a Registered Professional Land Surveyor for the State of Illinois and is not intended to be used as such. CCRPC, its officials, and its employees do not accept any liability for any discrepancies, errors, or variances that may exist._