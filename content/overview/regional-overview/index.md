---
title: "Regional Overview"
draft: false
weight: 20
bannerHeading: Regional Overview
bannerText: >
  The REF covers 998 square miles in central Illinois, the entirety of Champaign County.
---

The REF considers a regional view of ecological, social, and regulated-substance
resources encompassing the entirety of Champaign County. Champaign County is
located in the heart of east-central Illinois, surrounded by Piatt, Ford,
Vermilion, and Douglas Counties. It is the fifth largest county in the State of
Illinois, with approximately 638,528 acres, or 998 square miles. Champaign and
Urbana, the primary cities of Champaign County, are approximately 136 miles
south of Chicago, 120 miles west of Indianapolis, Indiana and 165 miles
north-northeast of St. Louis, Missouri. Champaign County is part of District 5
of both the Illinois Department of Transportation and Environmental Protection
Agency.  Within Champaign County, the Metropolitan Planning Area (MPA) serves as
a long range transportation planning boundary that encompasses the
Champaign-Urbana Urbanized Area as delineated by the 2010 U.S. Census.  Besides
the cities of Champaign and Urbana, the MPA includes the Villages of Savoy,
Tolono, and Mahomet. 

{{<image src="Regional_Overview.png"
  link="/destination"
  alt="Figure describes relationship between mobility and access"
  attr="Regional Overview" attrlink=""
  position="right">}}

Over 2,600 miles of roadways cross Champaign County, making it a statewide
transportation hub. These roadways consist of federal and state highways,
(Interstate Highways 57, 72, and 74; Federal Highways 45, 136, and 150) and
county and township roads. Many miles of railroads provide freight service as
well as passenger travel. The Willard Airport, south of Savoy, and several
smaller airports throughout the County provide commercial and private passenger
air service.  Originally an expanse of marshy wetlands complemented with
woodlands and tall grass prairie, Champaign County experienced mass drainage
efforts in the 1800s leading to the proliferation of agricultural land seen
today.  The soil of the county is its most valuable natural resource, providing
the base for the mainstay of the economy, agriculture. 

Champaign County has a population of approximately 209,689 according to the
2015-2019 American Community Survey 5-Year Estimates.  With an estimated [4.3
percent](https://www.census.gov/quickfacts/fact/table/champaigncountyillinois/PST045219)
population rise since the 2010 Census, consistent growth is expected to
continue. Most residents are between [15-29 years
old](https://ccrpc.org/programs/workforce-development/employer-and-business-services/champaign-county-profile/#commuting-characteristic),
much due to the presence of the University of Illinois Urbana-Champaign population, with a fairly
even split between male and female populations.   The majority of the County’s
population is white (73 percent), and the second largest race group is African
American, making up approximately 13 percent.  A significant Asian population
exists (11%), and approximately six percent of the population is Hispanic.
Median household income stands at just over $52,000 with most recent available
data listing a 3.89 percent unemployment rate.
