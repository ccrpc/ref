---
title: "What is REF?"
draft: false
weight: 10
bannerHeading: What is REF?
bannerText: >
  The REF serves as a preliminary screening tool for environmental review and analysis that improves overall project sustainability.
---

The Regional Environmental Framework (REF) is a centralized resource intended to
strengthen environmental stewardship in the transportation planning process.
The REF helps local planners, agencies, and engineers make informed decisions
about transportation projects by providing data and analysis regarding
ecological, cultural, and regulated-waste resources across Champaign County.  As
a preliminary screening tool, the REF allows users to identify
compliance-triggering and ecologically vulnerable resources within a project
location and incorporate them early into project planning, laying the groundwork
for potential National Environmental Policy Act and Illinois Dept. of
Transportation (IDOT) environmental review processes.  

The REF is made up of several components: the REF Report, the Web-based REF, and
the Interactive Web Mapping Tool. The REF began development in May 2019, with
its first phase report published in November 2020. The [first phase
report](https://ccrpc.org/documents/ref/) contains comprehensive documentation
on the ecological and compliance-triggering resources within Champaign County
supplemented with maps detailing the spatial layout of each feature.  The second
phase, the Web-based REF, began in January of 2021. This web platform is the
Web-based REF, which summarizes data and analyses from the REF Report and makes
information more accessible in a user-friendly, online resource.  The second
phase also includes the development of a composite score within the Interactive
Web Mapping Tool which will provide an initial analysis of the potential
vulnerability of locations by aggregating attributes of listed environmental
features, indicating the relative potential for ecological loss and
environmental review for a project in a given area. When used in conjunction,
the components of the REF serve as a powerful preliminary screening tool in
initial preparation for an environmental review or analysis and improve the
overall sustainability of transportation planning throughout the region. 
