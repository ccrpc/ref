---
title: "Overview"
draft: false
menu: main
weight: 10
bannerHeading: Overview
bannerText: >
  Learn how the REF works as a centralized resource for strengthening environmental stewardship in transportation planning combining a regional view of ecological, social, and regulated-substance resources that can be seen and analyzed using an Interactive Map.
---

This section contains an executive summary of the REF, including an overview of the planning-region, and access to the REF Interactive Map.