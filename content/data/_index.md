---
title: "Composite Score"
draft: false
menu: main
weight: 40
bannerHeading: Composite Score
bannerText: >
  The Composite Score of Ecological Vulnerability serves as a preliminary screening tool in initial preparation for an environmental review or analysis and facilitates sustainable planning.
---

This section contains the methodology for the REF Composite Score, a classification system based on the potential ecological vulnerability of a Census block within Champaign County.