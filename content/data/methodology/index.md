---
title: "Methodology"
draft: false
weight: 10
bannerHeading: Methodology
bannerText: >
  The Composite Score of Ecological Vulnerability serves as a preliminary screening tool in initial preparation for an environmental review or analysis and facilitates sustainable planning.
---

The REF Composite Score is a classification system based on the ecological
vulnerability of an area.  This system will consist of a Composite Score
aggregating attributes of identified environmental features, the Ecological
Vulnerability Layers (EVLs), relevant to requirements of state and federal
environmental reviews and overall ecological integrity of Champaign County. This
Composite Score (hereafter referred to as the Ecological Vulnerability Score)
links transportation planning with environmental review processes by considering
ecological vulnerability in the context of review-triggering conditions.  Final
scores indicate the combined potential for ecological loss and environmental
review for a project in a given location.  The Composite Score is not a
predictive model of outputs reflecting the ecological effects and/or impacts of
a proposed infrastructure project, but it will serve as a preliminary screening
tool in initial preparation for an environmental review or analysis and improve
the overall sustainability of transportation planning.

Other Definitions:
- Ecological: The relationship of the interconnected community of living things,
  including humans, and the physical environment within they interact [(FHWA, 2010)](https://www.environment.fhwa.dot.gov/env_initiatives/eco-logical/report/eco_app_a.aspx).  
- Vulnerability: The potential for ecological loss (negative impact) related to
  transportation development projects [(Adger, 2006)](https://www.sciencedirect.com/science/article/abs/pii/S0959378006000422?via%3Dihub).
- Ecological Vulnerability Layer (EVL): Spatial data layer containing features
  and attributes representing a singular type of resource included in the
  Ecological Vulnerability Score.  For example, wetlands and soil types are two
  distinct EVL’s, with attributes specific to the resource, but both
  contributing to overall ecological vulnerability.

## Assumptions, Limitations
- Humans are considered an integral part of the ecological community. Thus,
  vulnerabilities of the human-landscape must be considered to reflect the
  co-operative mechanisms between people and the land and offer guidance for
  meeting a range of ecological situations [(Leopold,
  1949)](https://doi.org/10.1016/j.gloenvcha.2006.02.006). For example, Cultural
  Resources are included in the Ecological Vulnerability Score, not due to their
  natural quality but because of their importance to human-landscape.
- The Ecological Vulnerability Score does not cover all potentially vulnerable
  resources. Users should consider all resources within their project location,
  even those not covered in the REF, for the most comprehensive environmental
  evaluation. 
- EVL scores are based on extensive research, literature review, and local
  context, but do contain subjectivity.
- The Ecological Vulnerability Score is a preliminary screening tool to
  highlight the general potential vulnerability of resources and is not based on
  a peer-reviewed methodology.
- Data sources feature varying timeframes and accuracy levels, however
  generalized findings are still reliable and valid despite these limitations.
  Data will be updated and incorporated into the score regularly.  
- It is assumed that proximity to a project’s geographic boundaries will result
  in a negative impact.  Impacts are treated uniformly with no judgement on
  severity.
- Special waste is not included despite having relevance to both the natural and
  human landscapes.  This is due to a combination of data limitations and
  scoring bias.  Locations of special waste sites are approximate, not exact, so
  their presence could improperly affect final scores of a location.  Inclusion
  would also bias scores heavily against the Metropolitan Planning Area (MPA)
  due to the prominence of sites, promoting ecologically damaging “greenfield”
  development outside the MPA. Furthermore, the existing literature does not
  support a sound way to meaningfully incorporate the ecological vulnerability
  of special waste in an Ecological Vulnerability Score.
- Agricultural Land and Historic Districts were not included in the original REF
  Phase 1 report but were included in the Ecological Vulnerability Score due to
  Steering Committee input, and further review of the literature.
- Vulnerability is described by its three constituting elements, only two of
  which are represented by the Ecological Vulnerability Score [(Weißhuhn et al., 2018)](https://doi.org/10.1016/j.gloenvcha.2006.02.006):
>* “Exposure”: The probability of a stressor occurring. Exposure is considered to occur when a project area is in proximity to an EVL. 
>* “Sensitivity”: measure of susceptibility (vulnerability) to a stressor. Sensitivity is considered through the selection criteria for EVLs.  Only resources that are susceptible (vulnerable) to impact are considered (See Table 1).
>* “Adaptive Capacity”: ability to cope with the stressor and its consequences. Adaptive Capacity is not considered within the Composite REF Score.

## Methods
The goal for this regional assessment is to characterize the general ecological
vulnerability of Champaign County both for potential impacts caused by
transportation projects and for triggering environmental review processes.
First, an in-depth literature review was conducted to identify methods to guide
development of feature-based composite scoring.  Key resources are the North
Central Texas Council of Government’s [“Regional Ecosystem
Framework”](https://www.nctcog.org/trans/quality/environmental-coordination/regional-ecosystem-framework),
North Carolina Division of Water Quality’s [“Yadkin Pee-Dee River Basin Priority
Watershed
Atlas”](https://www.nctcog.org/trans/quality/environmental-coordination/regional-ecosystem-framework)
(YPD Atlas), and EPA’s
[NEPAssist](https://nepassisttool.epa.gov/nepassist/nepamap.aspx?wherestr=champaign+county)
tool.  The Champaign County REF Ecological Vulnerability Score methodology
deviates slightly from literature to overcome unique barriers in development,
but important components and decision-making processes were retained.  

Based on the assumptions and limitations, EVLs were selected from the [REF
Report](https://ccrpc.org/documents/ref/) using a selection criteria.  A scoring
system was then devised using EVL attributes, literature reviews, and best
judgment by planning staff.   Scores were added as an attribute to each EVL,
which were then used as an input into a GIS union tool. The Ecological
Vulnerability Score layer was then calculated by summing all of the EVL scores
for the resulting union polygons. The Ecological Vulnerability Scores of each
polygon were then aggregated to the Census Block level using the aerial
interpolation method [(Flowerdew et al.,
1991)](https://link.springer.com/article/10.1007/BF01434424).  With Ecological
Vulnerability Scores aggregated to an appropriate level, the ecological
vulnerability of Champaign County was characterized and analyzed. The following
paragraphs provide a detailed walk-through of these steps.

### Layer Selection
Ecological Vulnerability Layers (EVLs) were chosen using a set of selection
criteria developed by CCRPC staff.  These criteria tend to favor
review-triggering and regulatorily significant resources, but others that do not
fall under those categories, like Soil Development Potential, are still integral
to a holistic Composite Score of Ecological Vulnerability.  EVLs must satisfy
3/5 of these criteria for selection.  Some resources that were included in the
REF Report did not satisfy 3/5 of the criteria and so were not scored. These
resources are Topography, Watersheds, the Mahomet Aquifer, Special Waste, and
Air Quality, Light Pollution, and Noise Pollution.  For more information on
these resources, consult the REF Report.  The EVL selection criteria are as
follows:

<rpc-table
url="EVL.csv"
table-title="EVL Selection Criteria"
source=""
source-url="">
</rpc-table>

To see this criteria in action, consider the EVL, Agricultural Land:
- Will the identified resource layer impact or be impacted by transportation development? Yes. Agricultural land is often bought or acquired through eminent domain for conversion purposes.
- Do either the IDOT or NEPA identify the layer as review-triggering based on ecological condition or risk? Yes. Best Prime and Prime farmland (attributes of Agricultural Land) are both required to be considered during IDOT and NEPA reviews.  Beyond that, several regulatory requirements protect Best Prime and Prime farmland from conversion.
- If not identified as review-triggering, does the literature support inclusion of the layer? N/A Agricultural Land is review-triggering. Agricultural Land is still supported by the literature.  Stakeholder input and the NCTCOG REF confirm its inclusion.
- Can the layer be aggregated in the Ecological Vulnerability Score in a meaningful way that accurately represents the resource? Yes.  The five, tiered classes of Agricultural Land fit into the 0-5 scoring system so as to correctly represent the differences in protection and relative importance of each.
- Is the layer part of a publicly available dataset with clear connection to socio-ecological systems? Yes. Agricultural Land was obtained from Natural Resource Conservation Service datasets and includes important soil characteristics that impact food and economic systems, as well as ecological systems.

EVL selection relies on demonstrated interrelation between the natural and built
environment and relevance to regulatory review of transportation projects. Selected EVLs and corresponding data sources are provided below.
- Soil Development Potential - NRCS, [Web Soil Survey](https://websoilsurvey.sc.egov.usda.gov/App/WebSoilSurvey.aspx) (2020)
- Hydric Soil - NRCS, Web Soil Survey (2020)
- Agricultural Land - NRCS, [Soil Data Access (SDA) Prime and other Important Farmlands](https://www.nrcs.usda.gov/Internet/FSE_DOCUMENTS/nrcseprd1338623.html#reportref[) (2019), and CCRPC, [Land Resource Management Plan](https://ccrpc.org/documents/champaign-county-land-resource-management-plan/) (2010)
- Surface Water - USGS, [National Hydrography Dataset](https://www.usgs.gov/national-hydrography/national-hydrography-dataset) (2020)
- Flood Hazard Area - USGS, National Hydrography Dataset (2020)
- Biologically Diverse Streams - IDNR, [Biological Stream Ratings](https://www2.illinois.gov/dnr/conservation/BiologicalStreamratings/pages/default.aspx) (2020)
- Wetlands - USFWS, [National Wetland Inventory](https://www.fws.gov/program/national-wetlands-inventory/wetlands-data) (2020)
- 303(d) List Streams - IEPA, [Integrated Water Quality Report and Section 303(d) List](https://www2.illinois.gov/epa/topics/water-quality/watershed-management/tmdls/Pages/303d-list.aspx) (2018)
- Historic Places - NPS, [NRHP](https://www.nps.gov/subjects/nationalregister/database-research.htm) (2020)
- Cemeteries - CCGS, [Cemetery List](http://graveyards.com/Illinois/Champaign) (2020)
- Archaeological Area - CCRPC, [Champaign County Greenways and Trail Plan](https://ccrpc.org/documents/active-choices-champaign-county-greenways-trails-plan-2014/#:~:text=The%20Champaign%20County%20Greenways%20%26%20Trails,to%20update%20the%202004%20plan.) (2014)
- Wooded Area - CCGIS, Wooded Areas (2020)
- Conservation Reserve Program - CCGIS, [Conservation Reserve Program](https://maps.ccgisc.org/public/Default.aspx) (2020)
- INAI Sites - IDNR, [Natural Heritage Database](https://www2.illinois.gov/sites/naturalheritage/DataResearch/Pages/Access-Our-Data.aspx) (2019)
- Conservation Easement - Holman, David. Illinois Protected Lands Geodatabase (2020)
- Nature Preserves - IDNR, [Natural Heritage Database](https://www2.illinois.gov/sites/naturalheritage/DataResearch/Pages/Access-Our-Data.aspx) (2019)
- Parks & Recreation Sites - CCGIS, Parks and Recreational Areas (2020)
- Grant Funded Sites (LAWCON, OSLAD, C2000/PFC, Ameren RTRP, IAW Environmental Grant, ACSP) - Village of Mahomet, Village of Rantoul, Champaign County Forest Preserve District, Urbana Park District (2020)
- Threatened & Endangered Species Observations - IDNR, List of Endangered and Threatened Species in Illinois (2020)


Some EVLs include sub-categories, but most are simply classified as either
present or not present in an area.  For example, there are no distinctions made
amongst wetland types: all wetland types are considered equally and carry equal
value.  However, other EVLs, such as Natural Areas contain sub-layer
distinctions that are represented in different valuations.  Wooded Areas are
valued differently than CRP Land, which is valued differently than INAI sites,
etc.  These distinctions can be seen in the following scoring section, and are
the product of literature review, Steering Committee input, and data analysis
performed by staff.  Additional information behind the scoring distinctions can
be found in the Scoring Rationale section for each EVL. 

### Scoring System
Informed by the scoring systems devised in the NCTCOG REF and YPD Atlas, EVLs
are scored from 0-5 based on their potential impact vulnerability.  Higher
scores indicate that many ecologically vulnerable resources are present at that
location, making it less suitable for transportation project development.  Lower
scores indicate that few or no (in the case of a zero score) ecologically
vulnerable resources are present at that location, making it more suitable for
transportation project development. Initial scores were assigned through a
combination of literature review, Steering Committee input, and best judgement
given available data. EVLs could be scored in three generalized ways:

#### Graduated Scoring
Scores are assigned sequentially with a 0 through 5 scoring range. In Graduated
Scoring, each score (0,1,2,3,4,5) has a corresponding criteria.  Graduated
scoring is used whenever possible and offers the greatest level of detail when
considering potential vulnerability. In some cases, different types, or
characteristics of an EVL easily allow for the use of graduated scoring.  For
example, Surface Water is scored by applying multi-ring buffers representing
five distances from a waterbody.  Scores reflect greater vulnerability as
buffers get closer to the waterbody.  Graduated scoring is applied to other EVLs
where the categories are more subjective.  For example, scores for the potential
vulnerability of Natural Areas and Parks and Rec sites are based on the
ecological and regulatory significance of the site type relative to one another.
Subjective decisions were still made using literature review, Steering Committee
input, and best judgement.  Several site types are grouped with scores of three,
four, and five.  

#### Binary Scoring
Scores are either 0 or 5 based only on presence of EVL in a location.  Binary
scoring is used when the most appropriate consideration is whether or not the
EVL is present in a location, and when graduated scoring is not possible.  An
example is 303(d) List Streams.  There is no logical way to score Champaign
County 303(d) List Streams based variances on impairment types or designated
use. Generally, the most main concern in preliminary screening is simple
resource identification. Therefore, when present, an impaired stream is scored
as a 5.  For other EVLs using binary scoring, no characteristic or type
distinctions can be made logically.  An example is Hydric Soil.  In this case,
there are no defining characteristics other than hydric status.  Is there hydric
soil present? If yes, then a five is scored for that area.  If no, then a zero
is scored for that area.

#### Attribute-Based Scoring
Scores are based on sub-layer attributes but still range from 0-5.
Attribute-based scoring is used when neither graduated nor binary scoring can be
applied.  Attribute-based scoring differs from Graduated Scoring because there
is not a sequential progression of scores (not criteria for each score
0,1,2,3,4,5).  Jumps in scores may be necessary when a sub-layer type or other
defining attributes of an EVL cannot be grouped into five categories or requires
more nuanced consideration than only presence in a location.  For example, Flood
Hazard Area has two types, 100-year and 500-year floodplains.  These types are
distinct and need distinct scores.  There is no logical way to apply either
graduated (0-5) or binary scoring (0 or 5) to Flood Hazard Area. So, best
planning judgement is used to assign scores that reflect the important
distinctions between types and the role these types play in preliminary
screening and environmental review processes: zero for no floodplains, four for
the 500-year floodplain, and five for the 100-year floodplain.  Another example
is Soil Development Potential, where the same principles apply.  However, the
role Soil Development Potential plays in preliminary screening and environmental
review processes is not equivalent to that of Flood Hazard Areas.  As a result,
Soil Development Potential is scored lower despite having the same number of
categories, reflecting these differences: zero for Not Limited, one for Somewhat
Limited, and two for Very Limited.  Despite individual EVL scoring variances,
the scoring range of 0 for no ecological vulnerability, and 5 for highest
ecological vulnerability was maintained throughout.

<rpc-table
url="score.csv"
table-title="REF Composite Scoring System"
source=""
source-url="">
</rpc-table>

### Vector-Union Method
EVLs are created, obtained, or edited to all be in vector geometry (point, line,
and polygon).  In order for the spatial analysis to work, all EVLs need to be in
only polygon geometry. For all point features, such as NRHP sites or
certain Parks and Recreation sites, corresponding land parcels are selected to
ensure every feature is in polygon geometry.  Scores are then
added to each EVL feature attribute table according to the scoring system (Table
3).  Figure 1 shows the Flood Hazard Area layer data before and after being
scored based on the criteria. All polygons of the 100-year floodplain are given
a score of five, all polygons in the 500-year floodplain are given a score of
four, and all other polygons in the layer are zero. 

{{<image src="flood.jpg"
  link="/destination"
  alt="Figure shows Flood Hazard Area Scores"
  attr="Flood Hazard Area Scores" attrlink=""
  position="right">}}

This concept is applied to each EVL.  Then, the Union tool is used to combine
all EVLs into a single layer containing all features.  When features from
different EVLs overlap, a new feature is created for the overlapping area that
contains the attributes from all overlapping layers [(ESRI, 2021)](https://desktop.arcgis.com/en/arcmap/latest/tools/analysis-toolbox/union.htm).  The resulting
layer computes a geometric union of the layers that contains individual scores
from all layers.  Scores are then summed to create an Ecological Vulnerability
Score for each geometric union across the entire county.  Figure 2 visually
depicts this process for three example EVLs. The resulting Ecological
Vulnerability Score map shows the new features created by the Union tool with
the summed scores from all overlapping EVLs. Figure 3 depicts the Ecological
Vulnerability Score map, with darker colors indicating higher scores (more
vulnerable areas).  

{{<image src="flow_chart.jpg"
  link="/destination"
  alt="Figure shows Ecological Vulnerability Score Calculation Process"
  attr="Ecological Vulnerability Score Calculation Process" attrlink=""
  position="full">}}

## Ecological Vulnerability Score
This section analyzes the resulting Ecological Vulnerability Scores to help
identify the most and least potentially vulnerable areas in Champaign County
(most and least suitable for transportation projects) and help identify
potential future impacts that transportation projects may have on these
ecosystems. By doing this, transportation planning can be better linked to the
NEPA and IDOT review processes, resulting in more streamlined reviews and
stronger environmental outcomes.
 
The Ecological Vulnerability Scores within the Champaign County range from 0 to
50 and are depicted from lowest to highest vulnerability.  The resulting
Ecological Vulnerability Score map is accurate in identifying areas of potential
vulnerability for environmental impacts that could require further review, based
on several measures.  For example,  the Ecological Vulnerability Score
methodology follows recognized guidelines, like those found in the IDOT Bureau
of Design and Environment (BDE) Manual.  The Ecological Vulnerability score
captures the majority of review-triggering resources and resources of concern
listed in the [IDOT ESR](http://apps.dot.illinois.gov/environment/onlineforms.asp), Phase 1 Corridor Report (Ch. 12 of the [IDOT BDE Manual](https://idot.illinois.gov/Assets/uploads/files/Doing-Business/Manuals-Guides-&-Handbooks/Highways/Design-and-Environment/Design%20and%20Environment%20Manual,%20Bureau%20of.pdf)),
and environmental resources to consider for non-federal projects listed in Ch.
22 of the BDE Manual.  Beyond IDOT, this methodology follows other recognized
methods, such as the [NCTCOG REF](https://www.nctcog.org/trans/quality/environmental-coordination/regional-ecosystem-framework), NCDWC [Yadkin-Pee Dee River Basin Watershed Atlas](https://www.ptrc.org/Home/ShowDocument?id=1019), EPA [NEPAssist](https://nepassisttool.epa.gov/nepassist/nepamap.aspx?wherestr=champaign+county), FHWA [Eco-Logical](https://www.environment.fhwa.dot.gov/env_initiatives/eco-logical.aspx), and NEPA review guidelines.  Staff has
also acquired and incorporated feedback from planners, engineers, ecologists,
and other industry experts through several Steering Committee meetings.
Finally, it achieves the stated vision by identifying resources that are
review-triggering, relevant to the ecological integrity of the region, and
vulnerable to potential impact by transportation development projects.
For more information on validation of the Ecological Vulnerability Score, see
Validation Measures. 
 
{{<image src="Composite_Score.jpg"
  link="/destination"
  alt="Figure shows Champaign Ecological Vulnerability Score"
  attr="Champaign County Ecological Vulnerability Score" attrlink=""
  position="full">}}