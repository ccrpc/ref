---
title: "Score Rationale"
draft: false
weight: 30
bannerHeading: Score Rationale
bannerText: >
  Planning and Environmental Linkages offer opportunities to facilitate environmental reviews and improve the understanding of environmental conditions and project alternatives.
---

## Soil Development Potential
Soil Development Potential is included as an EVL due to
the interrelation of the natural environment and transportation infrastructure.
The success of a transportation project relies on the ability of the soil to
support infrastructure and maintenance activities.  This interrelation forms the
basis of ecology, as defined by the [FHWA](https://www.environment.fhwa.dot.gov/env_initiatives/eco-logical/report/eco_app_a.aspx).

<rpc-table
url="soil.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale ([NRCS, Soil Science Manual](https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/ref/?cid=nrcseprd1343020))
- Very Limited: 2
  - Soils in this class are members of the set of soils that are limited for use.
  - Limitations often require costly construction/design, or removal and replacement of the soil.
  - Despite the rating, these soils can be adapted for use, but the cost of overcoming the limitation is high.
  - These soils are usually hydric but not always, so the score cannot meet the same vulnerability status as confirmed hydric soils.

- Somewhat Limited: 1	
  - Soils in this class are partial members of the set of soils that have limitations.
  - Soils have certain undesirable properties or features.
  - While modification or maintenance is required for satisfactory performance, use typically does not involve exceptional risk or cost.

- Not Limited: 0
  - Soils in this class give satisfactory performance with little or no modifications. 

- Not Rated: 0
  - Soils in this class are usually special waste, urban soil, or have missing data needed to produce a rating.  
  - Special waste is a non-scored resource
  - No determinations can be made on development potential, so this class will not be scored.


## Hydric Soils
Hydric soils are those formed under conditions of saturation,
flooding, or ponding long enough during the growing season to develop anaerobic
conditions within the top 20 inches of soil depth [(Champaign County Soil Survey,1998)](https://www.nrcs.usda.gov/Internet/FSE_MANUSCRIPTS/illinois/IL019/0/champaign_IL.pdf). They are included as an EVL because they pose serious challenges to
infrastructure and maintenance activities, while also supporting growth and
regeneration of wetland vegetation.  Hydric soils are one of the three
characteristics used to identify wetlands, and so are used by scientists for
wetland delineation surveys during project development [(USGS, n.d.)](https://www.usgs.gov/news/science-snippet/wetlandwordhydricsoil#:~:text=In%20wetlands%2C%20hydric%20soil%20supports,for%20purposes%20like%20wetland%20delineation.).

<rpc-table
url="hydric.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale ([NRCS, Soil Science Manual](https://www.nrcs.usda.gov/wps/portal/nrcs/detail/soils/ref/?cid=nrcseprd1343020))

- Hydric: 5
  - Limited infiltration of water can cause surface ponding, flooding, weak structure, and are highly erodible. 
  - Not all soils that have very limited development potential are hydric, but all hydric soils have very limited development potential. So it is important to have both identified and scored due to these vulnerabilities.
  - Key resources for environmental surveying.
- Not Hydric: 0	
  - Soils not fitting the NRCS definition of hydric soils.
  - These soils may still have limited or very limited development potentials but will not face the level of vulnerability to surface ponding, flooding, structural failure, or erosion as hydric soils.


## Agricultural Land
Agricultural land is included as an EVL because it measures important soil
characteristics that impact food, economic, and ecological systems, is supported
by the literature, and also triggers environmental review [(USDA, n.d.)](https://www.nrcs.usda.gov/wps/portal/nrcs/detailfull/pr/soils/?cid=nrcs141p2_037285) [(Powers, 2013)](https://www.sciencedirect.com/referencework/9780123847201/encyclopedia-of-biodiversity).  Classes of agricultural land are determined by the Natural Resource Conservation Service (NRCS) and the Champaign  County Land Evaluation and Site
Assessment System [(LESA)](https://www.co.champaign.il.us/CountyBoard/ZBA/2012/120628_Meeting/120628lesadraft111117.pdf).  Champaign County LESA is a tool designed to provide
County officials with a systematic and objective means to numerically rate a
site or parcel in terms of its agricultural importance. Classes indicate soil
conditions that have varying levels of suitability for cultivation.  Regulatory
protections of farmland play a role in scoring, as well.  The Federal Farmland
Protection Policy Act of 1981 [(FFPPA)](https://www2.illinois.gov/sites/agr/Resources/LandWater/Documents/LESA.pdf) requires evaluation of federal agency actions to produce the least impact on farmland. At the state level, the
Farmland Preservation Act of 1982 [(FPA)](https://www2.illinois.gov/sites/agr/Resources/LandWater/Documents/LESA.pdf) directs an Illinois Dept. of Agriculture review of all state agency projects that may have a direct or indirect effect
upon the potential conversion of farmland in Illinois.  

<rpc-table
url="farmland.csv"
table-title=""
source=""
source-url="">
</rpc-table>
Score Rationale
- Best Prime Farmland: 5
  - Soils with the highest productivity scores based on the Champaign County [LESA](https://www.co.champaign.il.us/CountyBoard/ZBA/2012/120628_Meeting/120628lesadraft111117.pdf) system.
  - [Champaign County Zoning Ordinance](https://www.co.champaign.il.us/planningandzoning/PDF/forms/Ordinance_Zoning.pdf) lists rules and restrictions on the use of Best Prime Farmland for non-agricultural uses in Champaign County.
- Prime Farmland: 4
  - Land that has the best combination of physical and chemical characteristics for producing food, feed, forage, fiber, and oilseed crops and is available for these uses ([USDA, n.d.](https://www.nrcs.usda.gov/wps/portal/nrcs/detailfull/pr/soils/?cid=nrcs141p2_037285)). 
  - Protected under FFPPA and FPA.
  - Land is of major importance to meeting the Nation’s short- and long-range needs for food and fiber.
- Prime if Drained: 3	
  - Protected under FFPPA and FPA.
  - Only requires draining to reach Prime status.
- Prime if -: 2 	
  - Prime farmland if drained and either protected from flooding or not frequently flooded during the growing season ([USDA, n.d.](https://www.nrcs.usda.gov/wps/portal/nrcs/detailfull/pr/soils/?cid=nrcs141p2_037285)). 
  - Prime farmland if protected from flooding or not frequently flooded during the growing season ([USDA, n.d.](https://www.nrcs.usda.gov/wps/portal/nrcs/detailfull/pr/soils/?cid=nrcs141p2_037285)) 
  - Land requires more intensive corrective measures to reach Prime status than ‘Prime if Drained’.
  - Onsite evaluation required to determine whether hazards can be overcome by corrective measures.
- Farmland of Statewide Importance: 1	
  - Lands that are nearly as productive as Prime farmland and that economically produce high yields of crops when treated and managed according to acceptable farming methods ([IDOA, 2001](https://www2.illinois.gov/sites/agr/Resources/LandWater/Documents/LESA.pdf)). 
  - Protected under FFPPA and FPA.
- Not Prime: 0	
  - Land does not have notably important agricultural characteristics.
  - Not protected under FFPPA or FPA.


## Surface Water 
Surface Water, both perennial and intermittent, was scored using a tiered-buffer
system developed by the North Carolina Division of Water Quality for the
“Yadkin-Pee Dee River Basin Priority Watershed Atlas” [(YPD Atlas, 2010)](https://www.ptrc.org/home/showdocument?id=1019).  To
score surface water, the REF Composite Score uses this same method consisting of
tiered-buffers of different distances around the waterways to consider impacts.
The closer the buffer is to the waterway, the higher the score given. Buffer
distances are set using local ordinances, EPA guidelines, and consistency as
references. 

<rpc-table
url="buffer.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- 25ft buffer: 5
	- Projects within 25 feet of a waterway have the highest potential for impact.
  - 25-foot buffer start is based on the Champaign County [Storm Water Management and Erosion Control Ordinance](http://www.co.champaign.il.us/planningandzoning/PDF/forms/Ordinance_Storm_Water_Management_Erosion_Control.pdf), which prohibits discharge of wastewater within 25 feet of any stream.
- 50ft buffer: 4
  - 50-foot buffer is based on the same ordinance as the 25-foot buffer, which prohibits stockpiles of erodible material within 50 feet from the top bank of any stream ([USGS, n.d.](https://www.usgs.gov/news/science-snippet/wetlandwordhydricsoil#:~:text=In%20wetlands%2C%20hydric%20soil%20supports,for%20purposes%20like%20wetland%20delineation.)). 
- 75ft buffer: 3	
  - With the 25 and 50-foot buffers established in the literature, continued use of 25-foot intervals up to 100 feet was determined for consistency.
- 100ft buffer: 2
	- 25-foot interval for consistency.
- 200ft buffer: 1	
  - The jump to 200-foot buffer comes from section 14.3 of the Illinois Groundwater Protection Act ([IGPA](https://www2.illinois.gov/epa/topics/water-quality/groundwater/Pages/maximum-setback-zones.aspx)) which states that counties and municipalities utilizing any community water supply well are authorized to establish a minimum setback zone of 200 feet around wells.
- Above 200ft: 0	
  - Projects further than 200 feet from surface water are less vulnerable to impact and review.


## Flood Hazard Area
<rpc-table
url="flood.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- 100-Year Floodplain: 5	
  - 100-year floodplains have the highest potential for flooding in a given year of any area within the County.
  - All floodplains are required to be considered as part of NEPA and IDOT reviews.
  - [Executive Order 11988](https://www.fema.gov/executive-order-11988-floodplain-management) requires practicable alternatives to impacting floodplains.
 
- 500-year Floodplain: 4
	- 500-year floodplains have the second highest potential for flooding in a given year of any area within the County.
  - All floodplains are required to be considered as part of NEPA and IDOT reviews ([IDNR, 2020](https://www2.illinois.gov/dnr/INPC/Pages/default.aspx))([IDOT, 2017](http://apps.dot.illinois.gov/environment/onlineforms.asp)).

- No Floodplain: 0
	- If not present, the area is less vulnerable to flooding due to project activities.

## Biologically Diverse Streams (BDS)
<rpc-table
url="05.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- Present: 5	
  - BDS designation by [IDNR](https://www2.illinois.gov/dnr/conservation/BiologicalStreamratings/Documents/StreamRatingReportSept2008.pdf) denotes streams of uniquely high biodiversity
- Not Present: 0
	- If not present, the area is not vulnerable for impacts to Biologically Diverse Streams.

## Wetlands
<rpc-table
url="05.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- Present: 5	
  - Wetlands are protected under several regulations such as the [Interagency Wetlands Policy Act](https://www.aswm.org/pdf_lib/state_summaries/illinois_state_wetland_program_summary_083115.pdf) and [Section 404](https://www.youtube.com/watch?v=jPBV8x985p0) of the CWA.
  - Wetlands are required to be considered as part of NEPA and IDOT reviews.

- Not Present: 0
	- If not present, the area is not vulnerable for impacts to wetlands.

## 303(d) List Streams
<rpc-table
url="05.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- Present: 5	
  - 303(d) List Streams, or impaired streams, are required to be considered for [NEPA review](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf).
  - 303(d) List Streams are subject to pollution limits (TMDLs) that regulate their use to reach water quality standards set by the [Illinois EPA](https://www2.illinois.gov/epa/topics/water-quality/watershed-management/tmdls/Pages/303d-list.aspx).
 
- Not Present: 0
	- If not present, the area is less vulnerable to impacts to 303(d) List Streams

## Cultural Resources
All three types of cultural resources are included as EVLs because of their
connection to their importance to the human-landscape, as outlined in the
Assumptions and Limitations section of the REF Composite Score Methodology. All
of these cultural resources also are review-triggering or regulatorily
protected. No distinction can be made between cultural resource types as to
which are more or less vulnerable.
#### National Register of Historic Places sites and districts
<rpc-table
url="05.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- Present: 5	
  - NRHP sites and districts are protected under Section 106 of the [National Historic Preservation Act](https://www.youtube.com/watch?v=0ayArv5ZTAc).
  - NRHP sites and districts are required to be considered by both [NEPA](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf) and [IDOT](http://apps.dot.illinois.gov/environment/onlineforms.asp) reviews. 
- Not Present: 0
	- If not present, the area is likely not vulnerable for impacts to NRHP sites and districts.

#### Cemeteries
<rpc-table
url="05.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- Present: 5	
  - Cemeteries are protected under the [Illinois Skeletal Remains Act](https://efotg.sc.egov.usda.gov/references/public/IL/IL_Laws_on_Historic_Preservation.pdf).
  - Cemeteries are required to be considered by [NEPA](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf). 
 
- Not Present: 0
	- If not present, the area is not vulnerable for impacts to cemeteries.

#### Archaeological Areas
<rpc-table
url="05.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- Present: 5	
  - Archeological areas are required to be considered by both [NEPA](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf) and [IDOT](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf) reviews. 
- Not Present: 0
	- If not present, the area is not vulnerable for impacts to archaeological resources.

## Natural Areas
The Natural Areas EVL consists of both designated natural sites and parks and
recreation sites.  Both are important both to the ecology of the region, as well
as environmental review processes, and thus need to be considered in the REF
Composite Score. 
<rpc-table
url="natural.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
  - Nature Preserves: 5
    - Nature Preserves are legally protected by IDNR and the [Natural Areas Preservation Act](https://www2.illinois.gov/dnr/INPC/Pages/Protection.aspx) in perpetuity.
    - Highest quality natural area designation in the state.
    - Required to be considered by [NEPA](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf) and [IDOT](http://apps.dot.illinois.gov/environment/onlineforms.asp). 
  - INAI Sites: 4
    - Illinois Endangered Species Protection Act requires consultation for INAI sites due to their ecological sensitivity ([IDOT, 2012](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf)).
    - Sites inform [Illinois Nature Preserve Commission](https://www2.illinois.gov/dnr/INPC/Pages/Protection.aspx) designation. 
    - High quality natural area designation ([CCRPC, 2014](https://ccrpc.org/documents/active-choices-champaign-county-greenways-trails-plan-2014/)).
  - Conservation Easements: 4  
    - Legally binding protections that prohibit damaging land uses ([Schear, P., 2020](https://web.extension.illinois.edu/lcr/easement.cfm)).
    - Mostly permanent, but not always ([LTA, n.d.](https://www.landtrustalliance.org/what-you-can-do/conserve-your-land/questions)).
    - Main purpose is conservation of natural resources. 
    - Land has demonstrated natural resource value, uniqueness of property, in danger of development, or is part of a land trust’s mission ([EPA, 2021](https://www.epa.gov/system/files/documents/2021-11/bmp-conservation-easements.pdf)).
  - Forest Preserves: 4
    - Forest Preserves have significant recreational, educational, and conservation value ([CCFPD, 2010](https://www.ccfpd.org/Portals/0/Assets/PDF/Master-Plan-2010.pdf)).
    - While no federal or state legal protections exist, the top guiding principle of the [CCFPD Master Plan](https://www.ccfpd.org/Portals/0/Assets/PDF/Master-Plan-2010.pdf) is protection. Any potential impacts for forest preserves will be met with intense pressure from local agencies and the public.
  - Public Park: 3
    - Some public parks are managed for ecological value (Ex. Weaver Park).
    - May be protected under [Section 4(f)](https://www.environment.fhwa.dot.gov/env_topics/4f_tutorial/default.aspx) of Department of Transportation Act of 1966. 
  - Public/Private Recreational: 3
    - Public/private recreational sites have ecological value (Ex. Curtis Orchard) ([Demstihas, 2017](https://doi.org/10.1007/s13593-017-0422-1)).
    - Private property rights pose challenge to use of land.
  - CRP Land: 3
    - Environmentally sensitive land ([USDA, 2020](https://www.fsa.usda.gov/Assets/USDA-FSA-Public/usdafiles/FactSheets/crp-clear30-pilot.pdf)).
    - While land is in CRP, no uses of any kind can be authorized, including fencing or boundaries that prohibit wildlife access ([CRP, n.d.](https://www.benefits.gov/benefit/340)).
    - While protection is not in perpetuity, contracts are minimum 10 years. 
    - Considered “minimally protected” by [RES](https://res.us/state/illinois/). 
  - Wooded Area: 2
    - Private property rights may pose challenge to use of land.
    - No protections or designations of natural area quality.
    - Wooded areas can provide some ecological value, but not generalizable across Champaign County sites.
    - Greater ecological value than golf course (RES, 2022).
  - Public Golf Course: 1
    - Private property rights pose challenge to use of land.
    - Ecological value when compared to other lands with high human impact ([Colding, 2009](https://doi.org/10.1007/s10021-008-9217-1)).
    - Not as much ecological value as Wooded Area (RES, 2022). 
  - None: 0
    - If not present, the area is less vulnerable to natural areas impacts.


## Grant Funded Sites
<rpc-table
url="grant.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- LAWCON: 5	
  - All grant-funded sites must remain, in perpetuity, for public outdoor recreation.
  - Protected under [Section 4(f)](https://www.environment.fhwa.dot.gov/env_topics/4f_tutorial/default.aspx) of Department of Transportation Act of 1966.
- OSLAD, C2000/PFC, Illinois American Water Environmental Grant, and Ameren Right Tree Right Place: 4 
  - Indicate that site has received funding for ecological management
  - State and local protections
  - Steering  Committee input
- Audubon Cooperative Sanctuary Program for Golf: 3
  - Environmental certification program that helps golf courses promote sustainability by guiding conservation efforts onsite ([Audubon Society, 2022](https://auduboninternational.org/wp-content/uploads/2022/01/ACSP-for-Golf-Fact-Sheet-2022_Compressed.pdf)).
  - Steering Committee Input
- Not present: 0

The scoring of grant-funded sites will be additive to the Natural Areas & Parks
& Rec Sites layer.  This means that for each site that has also received grant
funding to perform ecological-based management work, additional points will be
added to that parcel.  Scores have been assigned to each grant program based on
Steering Committee input and research.

Some sites have received funding from more than one grant.  When that happens,
the higher-scoring source will be used.  To illustrate this, consider the
example of Weaver Park vs. Canaday Park.  Both are public parks, so
automatically start with a score of three.  However, the following table
outlines how grant-funding alters the scores:

Weaver Park has several grants in addition to being a public park.  LAWCON is
the highest scored funding source, so those points are added to the three it
gets for being a public park for a total of eight points. Canaday Park has not
had any funding for ecological-based management, and thus is only scored based
on its designation as a public park for a score of three points.

  - Weaver Park Total Score: 8 
    - Score for Natural Areas: 3 (Public Park)
    - Score for Grant Funded Sites: 5 (highest score is selected)
      - LAWCON: 5
      - OSLAD: 4
      - Illinois American Water – Environmental Grant: 3

  - Canaday Park Total Score: 3  
    - Score for Natural Areas: 3 (Public Park)
    - Score for Grant Funded Sites: 0 (no grant received)

## Threatened and Endangered Species Habitat
<rpc-table
url="05.csv"
table-title=""
source=""
source-url="">
</rpc-table>

Score Rationale
- Present: 5	
  - Indicates known habitat of Federal and state threatened and endangered species.
  - Protected under Federal and state Endangered Species Protection Acts ([EPA, 2018](https://www.epa.gov/endangered-species/about-endangered-species-protection-program))([IDNR, 2020](https://www2.illinois.gov/dnr/conservation/NaturalHeritage/Pages/Incidental-take-or-Endangered-SpeciesPermit.aspx)).
  - Required to be considered by [NEPA](https://www.idothsr.org/pdf/feis_vol_1/section_04.pdf) and [IDOT](http://apps.dot.illinois.gov/environment/onlineforms.asp). 
- Not Present: 0
  - If not present, the area is less vulnerable to threatened and endangered species habitat.
